#!/bin/bash

# This script is intended to be run on the Google Dataproc cluster master and
# workers when they are created

# More information on these initialization scripts on
# https://github.com/GoogleCloudPlatform/dataproc-initialization-actions

set -e

init_variables() {
    # Magic command to get the Google metadata of this cluster
    # ROLE is either Master or Worker
    ROLE=$(curl -f -s -H Metadata-Flavor:Google http://metadata/computeMetadata/v1/instance/attributes/dataproc-role)
    PROJECT_ID=$(gcloud config get-value project)
    DATE=$(date)
    WHEEL_DIR=$(/usr/share/google/get_metadata_value attributes/wheel_directory)
    REQUIREMENTS_FILE=$(/usr/share/google/get_metadata_value attributes/python_requirements)
}

install_python() {
    echo "Installing Python 3 and setup of Spark to use Python 3"
    apt-get -y --no-install-recommends install python3 python3-pip python3-tk
    echo "export PYSPARK_PYTHON=python3" | tee -a /etc/profile.d/spark_config.sh /etc/*bashrc /usr/lib/spark/conf/spark-env.sh
    # Fix the issue with the Python 3 seed
    echo "export PYTHONHASHSEED=0" | tee -a /etc/profile.d/spark_config.sh /etc/*bashrc /usr/lib/spark/conf/spark-env.sh
    echo "spark.executorEnv.PYTHONHASHSEED=0" | tee -a /etc/spark/conf/spark-defaults.conf
    # Install common dependencies
    # TODO: use a requirements.txt file
    easy_install3 --upgrade pip
    echo "pip is $(which pip) $(which pip3)"

    echo "installing requirements"
    gsutil cp ${REQUIREMENTS_FILE} /tmp/requirements.txt
    pip install -r /tmp/requirements.txt

    echo "installing wheels from $WHEEL_DIR"
    TMP_WHEEL_DIR=$(mktemp -d)
    gsutil -m cp ${WHEEL_DIR}/* ${TMP_WHEEL_DIR}
    pip install ${TMP_WHEEL_DIR}/*
    
    # (pip install \
    #      numpy==1.13.1 \
    #      pandas==0.20.3 \
    #      scipy==0.19.1 \
    #      retryable==2016.3.11 2>&1) \
    #     || echo "pip install failed"
    #pip install
}


common_install() {
    echo "Common configuration on both Master and Worker..."
    install_python

    echo "Timezone configuration"
    rm /etc/localtime
    ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

    echo "Common install scripts finished"
}

master_install() {
    echo "Master configuration..."
    # Configure the scheduler of Spark
    cat > /etc/spark/conf/fairscheduler.xml <<EOF
<?xml version="1.0"?>
<allocations>
  <pool name="production">
    <schedulingMode>FAIR</schedulingMode>
    <weight>2</weight>
    <minShare>2</minShare>
  </pool>
  <pool name="default">
    <schedulingMode>FAIR</schedulingMode>
    <weight>1</weight>
    <minShare>0</minShare>
  </pool>
  <pool name="test">
    <schedulingMode>FIFO</schedulingMode>
    <weight>1</weight>
    <minShare>0</minShare>
  </pool>
</allocations>
EOF
    echo "spark.scheduler.mode=FAIR" | tee -a /etc/spark/conf/spark-defaults.conf
    echo "spark.scheduler.allocation.file=/etc/spark/conf/fairscheduler.xml" | tee -a /etc/spark/conf/spark-defaults.conf

    # Dynamic allocation... as I understood it from:
    # https://www.slideshare.net/databricks/dynamic-allocation-in-spark
    #
    # Still needs to be tested. According to the docs, this needs the shuffle
    # service to be activated (see doc in
    # https://spark.apache.org/docs/latest/running-on-yarn.html#configuring-the-external-shuffle-service). Fortunately,
    # the default Google dataproc has this service already activated.
    cat >> /etc/spark/conf/spark-defaults.conf <<EOF
# Configuration for mitigation of [RES-254]
spark.executor.heartbeatInterval 30s

# Configuration for dynamic allocation
spark.dynamicAllocation.enabled true
spark.shuffle.service.enabled true
spark.dynamicAllocation.minExecutors 1
spark.dynamicAllocation.maxExecutors 10000
spark.dynamicAllocation.initialExecutors 1
spark.dynamicAllocation.executorIdleTimeout 60
spark.dynamicAllocation.executorBacklogTimeout 5
spark.dynamicAllocation.sustainedSchedulerBacklogTimeout 5
EOF
    cat >> /etc/spark/conf/log4j.properties <<EOF
# Shuffle service to reclaim executors (under evaluation, we are not sure this works)
log4j.logger.org.apache.spark.deploy.ExternalShuffleService=INFO
EOF
}

worker_install() {
    echo "Worker configuration..."
    # Install docker and Python 3
    apt-get install -y --no-install-recommends \
            apt-transport-https \
            ca-certificates \
            curl \
            software-properties-common \
            python3 
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian jessie stable"

    apt-get update

    apt-get install -y docker-ce

    # Configure the permissions for the yarn user so that it can access the
    # docker socket. The user that runs the code using docker is yarn.
    usermod -aG docker yarn

    # YARN is already running when the groups of user yarn are changed just
    # above. So we need to restart YARN for the new processes to have the
    # updated group info. Another solution would be to change the permissions of
    # the docker socket to that yarn group, but that would be probably a risky
    # idea.
    # UPDATE [Jan 12th 2018]: This doesn't work on preemptible workers because
    # they don't have the same services; apparently there is no nodemanager?
    # In any case, it seems this is not necessary and 
    if [[ $(systemctl is-active hadoop-yarn-nodemanager.service) == "active" ]]
    then
       echo "Restarting YARN to reload user/group configuration"
       systemctl restart hadoop-yarn-nodemanager.service
       echo "YARN has been restarted"
    fi

    # Install docker images.
    # [RES-211]: no longer needed, now the images are pulled by the worker as
    # jobs arrive
    # TODO: consider pre-fetching some common images? Perhaps in another time
#    # login to the Google Container Registry so that we pull all images
#    #sudo -H -u yarn
#    docker login -u _token -p "$(gcloud auth print-access-token)" https://gcr.io
#
#    # Pull all images so that they are available for the jobs
#    declare -a images=(
#        "gcr.io/${PROJECT_ID}/cloudts/interpreters/python:3.6"
#        "gcr.io/${PROJECT_ID}/cloudts/interpreters/neurort:3.1.1"
#        "gcr.io/${PROJECT_ID}/cloudts/interpreters/r:3.4"
#    )
#    for img in "${images[@]}"
#    do
#        docker pull $img
#    done

    # Install python dependencies:
    # (no longer needed, this is managed on the requirements-cluster.txt file)
    # docker: the docker-py package for interfacing Python with docker
    # easy_install3 --upgrade pip
    # pip install --upgrade docker

    echo "Finished Worker-specific actions"
}

LOGFILE=/var/log/init-output.txt
echo "Running processes:" >> ${LOGFILE}
ps aux >> ${LOGFILE}
init_variables >> ${LOGFILE} 2>&1

echo "[${DATE}]: Running common initialization script for project ${PROJECT_ID}" >> ${LOGFILE}
common_install >> ${LOGFILE} 2>&1

echo "[${DATE}]: Running initialization script for role ${ROLE} in project ${PROJECT_ID}" >> ${LOGFILE}
if [[ "${ROLE}" == 'Master' ]]
then
    master_install >> ${LOGFILE} 2>&1
elif [[ "${ROLE}" == "Worker" ]]
then
    worker_install >> ${LOGFILE} 2>&1
fi

echo "Finished initialization actions, sourcing /etc/profile" >> ${LOGFILE}
source /etc/profile
