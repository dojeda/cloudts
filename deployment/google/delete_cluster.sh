#!/bin/bash

if [ $# -ne 1 ]
then
    echo -ne "Error: Missing environment\nUsage: $0 environment\n\tWhere environment is prod or dev\n"
    exit 1
fi

ENV_ARG=$1
DIR=$(dirname "$0")

source ${DIR}/google_cloud_env_${ENV_ARG}.sh || exit

command -v gcloud 2>&1 > /dev/null || {
    echo "glcoud is not installed, install it from https://cloud.google.com/sdk/"
}

echo "Deleting cluster..."
gcloud dataproc clusters --project ${PROJECT_ID} delete ${CLUSTER_NAME}
