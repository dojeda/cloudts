#!/bin/bash

# Usage:
# source ./deployment/google/google_cloud_env_prod.sh

################################################################################
# Prod settings

# The current version of cloudts platform
export CLOUDTS_VERSION="0.2.2"
# The name of the Google Cloud Platform project
export PROJECT_ID="cloudts-176214"
# The google compute region
export REGION_NAME="europe-west1"
export ZONE_NAME="europe-west1-b"
# The bucket where the Data API will save its files
export DATA_BUCKET_NAME="cloudts-beta-data"
# The bucket where the static files of Django will be served
export STATIC_BUCKET_NAME="cloudts-beta-web"
# The bucket where the cluster stage files will be saved
export DATAPROC_STAGE_BUCKET_NAME="cloudts-beta-cluster-stage"
# The name of the dataproc cluster for the Job API
export CLUSTER_NAME="cloudts-beta-job-cluster"
# The number of workers on the dataproc cluster
export SPARK_CLUSTER_NUM_WORKERS=2
# The number of preemptible workers on the dataproc cluster
export SPARK_CLUSTER_NUM_PREEMPTIBLE_WORKERS=0
# The machine type for dataproc workers
export SPARK_CLUSTER_WORKER_MACHINE="n1-standard-4"
# The name of the service accounts that need bucket read/write permissions. This
# is the client_email on the credentials.json file. You can put more than one if
# they are separated by spaces
export SERVICE_ACCOUNTS="696539961693-compute@developer.gserviceaccount.com"
# The external IP address created for the CloudTS platform
export CLOUDTS_EXTERNAL_IP="35.195.70.232"
# NeuroRT license user and key
export NEURORT_LICENSE_USER=""
export NEURORT_LICENSE_KEY=""
