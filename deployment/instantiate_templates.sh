#!/bin/bash


PROG=$(basename $0)

usage()
{
  echo "${PROG} <environment-file>"
}

expand()
{
  local template
  template="$(cat $1)"
  eval "echo \"${template}\""
}

if [ $# != 1 ]; then
  usage
  exit 0;
fi

source "$1"

for template_file in $(ls kubernetes/*.tmpl.yaml); do
    target_file="kubernetes/instances/$(basename ${template_file} .tmpl.yaml).yaml"
    echo "Instantiating ${template_file} to ${target_file}"
    expand ${template_file} > ${target_file}
done
