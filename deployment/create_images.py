# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
# from Mensia Technologies SA.
# ###################################
""" A Python script that builds the Docker images of CloudTS """

import argparse
import os
import sys
import subprocess
import shlex
import time


def _get_version():
    try:
        import cloudts
        return cloudts.__version__
    except ImportError as ex:
        raise Exception('Could not determine CloudTS version') from ex



def main():

    parser = argparse.ArgumentParser(description='Build and push Docker images for CloudTS')
    parser.add_argument('--registry', type=str, required=False, default=None,
                        help='Registry where the images will be pushed (e.g. gcr.io)')
    parser.add_argument('--version-tag', type=str, required=False, default='',
                        help='Name of the version tag')
    parser.add_argument('--latest', action='store_true', required=False, default=False,
                        help='Tag these builds as [latest]')
    parser.add_argument('--project-id', type=str, required=False,
                        default=os.getenv('PROJECT_ID'),
                        help='Google Cloud project name. If not set, the environment '
                        'variable PROJECT_ID will be used')
    parser.add_argument('--include', type=str, nargs='*', default=None,
                        help='Manually create only these images')

    opts = parser.parse_args()

    # Check that the PROJECT_ID variable is set
    project_id = opts.project_id
    if project_id is None:
        print('You need to set the --project-id option or PROJECT_ID environment variable')
        raise ValueError('Invalid PROJECT_ID')

    parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    version = opts.version_tag

    if '+' in version:
        print('WARNING: ', end='')
        print('Since there is a "+" in the version tag name, this scripts thinks '
              'you are using an untagged commit to generate the '
              'Docker images for CloudTS. If this is for development purposes, '
              'this is ok. But if you are trying to generate Docker images for '
              'deployment purposes, you should stop right now, commit, tag and '
              'then generate you Docker images again.'
              'Waiting 5s before continuing...')
        time.sleep(5)
        version = version.replace('+', '.')

    containers = [
        # Name, Dockerfile, context, push to server
        ('gcloud', 'docker/gcloud/Dockerfile', 'docker/gcloud', False),
        ('db', 'docker/db/Dockerfile', 'docker/db', True),
        ('nginx', 'docker/nginx/Dockerfile', 'docker/nginx', True),
        ('web', 'docker/web/Dockerfile', parent_dir, True),
        ('rabbitmq', 'docker/rabbitmq/Dockerfile', 'docker/rabbitmq', True),
    ]

    for name, dockerfile, context, push in containers:
        if opts.include is not None and name not in opts.include:
            print('Ignoring {} since it is not in --include'.format(name))
            continue
        print('Building {0}...'.format(name))
        cmd = ['docker build']
        if version:
            cmd.append('--tag cloudts/{name}:{version}')
        if opts.latest:
            cmd.append('--tag cloudts/{name}:latest')
        if opts.registry is not None:
            if version:
                cmd.append('--tag {registry}/{project_id}/cloudts/{name}:{version}')
            if opts.latest:
                cmd.append('--tag {registry}/{project_id}/cloudts/{name}:latest')

        cmd.append('-f {dockerfile} {context}')
        cmd = (' '.join(cmd)).format(registry=opts.registry,
                                     name=name, dockerfile=dockerfile,
                                     version=version, project_id=project_id,
                                     context=context)
        print('Running', cmd, sep='\n')
        retcode = subprocess.call(shlex.split(cmd))
        if retcode:
            sys.exit(retcode)

    if opts.registry is not None:
        print('Images build, uploading to Google Cloud...')
        for name, _, _, push in containers:
            if not push: continue
            if opts.include is not None and name not in opts.include: continue
            print('Pushing {0}...'.format(name))
            cmd = ['gcloud docker --project {project_id} -- push']
            cmd.append('gcr.io/{project_id}/cloudts/{name}:{version}')
            if version:
                cmd_fmt = (' '.join(cmd)).format(name=name, version=version,
                                                 project_id=project_id)
                print('Running', cmd_fmt, sep='\n')
                retcode = subprocess.call(shlex.split(cmd_fmt))
                if retcode:
                    sys.exit(retcode)

            if opts.latest:
                cmd[-1] = 'gcr.io/{project_id}/cloudts/{name}:latest'
                cmd_fmt = (' '.join(cmd)).format(name=name,
                                                 project_id=project_id)
                print('Running', cmd_fmt, sep='\n')
                retcode = subprocess.call(shlex.split(cmd_fmt))
                if retcode:
                    sys.exit(retcode)

if __name__ == '__main__':
    main()
