# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""
Singleton pattern as a metaclass in Python

Based on https://stackoverflow.com/a/6798042/227103

Example:

    class Name(metaclass=Singleton):
        ...

"""

from collections import namedtuple
from os import getpid

import logging


logger = logging.getLogger(__name__)


class Singleton(type):
    _instances = {}
    _entry_type = namedtuple('SingletonEntry', ('instance', 'args', 'kwargs'))
    def __call__(cls, *args, **kwargs):
        entry = cls._instances.get(cls, None)
        #logger.debug('instances of pid={}: {}'.format(getpid(), cls._instances))
        # if entry is None:
        #     logger.debug('New instance of {} because it doesnt exist'.format(cls.__name__))
        # elif entry.args != args or entry.kwargs != kwargs:
        #     logger.debug('New instance of {} because args changed'.format(cls.__name__))
        # else:
        #     logger.debug('Reusing singleton of {}'.format(cls.__name__))
        if entry is None or entry.args != args or entry.kwargs != kwargs:
            entry = Singleton._entry_type(instance=super(Singleton, cls).__call__(*args, **kwargs),
                                          args=args,
                                          kwargs=kwargs)
            cls._instances[cls] = entry
        return entry.instance

