# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


from django.apps import AppConfig

from .backends import get_backend


class JobAPIConfig(AppConfig):
    name = 'cloudts.api.job'
    verbose_name = 'CloudTS job API'
    label = 'cloudts_job'

    def ready(self):
        # initialize backends if necessary
        #print('Initializing job backend...')
        #data_backend = get_backend()
        pass # maybe not needed if we use a singleton approach
        # consider adding --noreload on production so this does not run twice
