# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" Code that generates atomic jobs using ncvt """

import json
import logging
import os
import re
import tempfile

import ncvt.config
import ncvt.scheduler
import ncvt.dag


logger = logging.getLogger(__name__)


def get_jobs(sequence, database, configuration):
    # Make a temporal directory for the NCVT main path; where all NCVT files
    # will be saved. We are interested in the json files for the jobs
    tmpdir = tempfile.TemporaryDirectory()
    configuration['main_path'] = tmpdir.name

    # Load and update the NCVT configuration
    base_config = ncvt.config.load_config()
    base_config.update(configuration)

    # Save the database and sequence files
    os.makedirs(os.path.join(tmpdir.name, 'cloudts'))
    sequence_file = os.path.join(tmpdir.name, 'cloudts', 'sequence.json')
    database_file = os.path.join(tmpdir.name, 'cloudts', 'database.csv')
    with open(sequence_file, 'w') as f:
        json.dump(sequence, f)
    database.to_csv(database_file, sep=configuration['csv_separator'], index=False)

    scheduler = ncvt.scheduler.Scheduler(sequence_file, database_file,
                                         configurable=base_config)
    job_list, remaining = next(scheduler.schedule())
    job_files = [j[0] for j in job_list]
    # calculate dependency graph
    jobs_order = ncvt.dag.dependency_order(job_files)

    # gather result as list of stages,
    # where each stage is a list of (filename, json_content)
    jobs = []
    for stage in jobs_order:
        jobs_stage = []
        for job in stage:
            job_filename = os.path.join(scheduler.jobs_dir, job)
            job_basename = os.path.basename(job_filename)
            with open(job_filename, 'r') as f:
                content = json.load(f)
            # # Separate PySpark jobs so that they are alone in a single stage
            # if content['Interpreter'] == 'PySpark':
            #     jobs.append([(job_basename, content)])
            # else:
            #     jobs_stage.append([(job_basename, content)])
            jobs_stage.append((job_basename, content))
        if jobs_stage:  # Could be empty if only a PySpark job exists in this stage
            jobs.append(jobs_stage)

    # The remaining sequence should be a "ProcessingSequence": ... object
    if remaining:
        remaining = {'ProcessingSequence': remaining}
    return jobs, remaining
