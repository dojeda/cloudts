# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
import cloudts

__version__ = cloudts.__version__
default_app_config = 'cloudts.api.job.apps.JobAPIConfig'
