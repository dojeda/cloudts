# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

""" Serializers for Django REST Framework in CloudTS Job API """

import collections
import json
import logging

from rest_framework import serializers
from django_celery_results.models import TaskResult

from cloudts.api.job.models import JobRequest
from cloudts.api.data.models import Workspace


logger = logging.getLogger(__name__)



class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField

    This serializer is needed to make JSONFields writable.

    """
    def to_internal_value(self, data):
        """ Transform the incoming primitive data into a native value. """
        return data

    def to_representation(self, value):
        """ Transform the outgoing native value into primitive data. """
        if isinstance(value, dict):
            return value
        else:
            return json.loads(value, object_pairs_hook=collections.OrderedDict)



# class JobSerializer(serializers.ModelSerializer):
#     """ Serializer for Job operations """

#     url = serializers.HyperlinkedIdentityField(view_name='job-detail')

#     class Meta:
#         model = Job
#         fields = ('id', 'content', 'workspace', 'query', 'url')


# class JobDetailSerializer(serializers.ModelSerializer):
#     """ Serializer for Job detail requests """

#     owner = serializers.ReadOnlyField(source='owner.username')

#     class Meta:
#         model = Job
#         fields = '__all__' # all model fields will be included


class JobCreateSerializer(serializers.ModelSerializer):
    """ Serializer for Job creation """

    # source tells which attribute goes in field
    #owner = serializers.ReadOnlyField(source='owner.username')
    workspace = serializers.PrimaryKeyRelatedField(queryset=Workspace.objects.all())
    content = JSONSerializerField(required=True)
    #url = serializers.HyperlinkedIdentityField(view_name='job-detail')

    class Meta:
        model = JobRequest
        fields = ('id', 'workspace', 'query', 'content', ) #'url', )#'owner'
        # extra_kwargs = {
        #     'id' : {'read_only': True},
        #     'url': {'read_only': True},
        # }



class JobSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskResult
        fields = ('task_id', 'status')

class JobDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskResult
        fields = ('task_id', 'status', 'result', 'date_done', 'traceback',
                  'meta')

################################################################################
# Logs: commented; this still needs work
# class LogSerializer(serializers.ModelSerializer):
#
#     job_request = serializers.PrimaryKeyRelatedField(queryset=JobRequest.objects.all(),
#                                                      source='parent_job')
#
#     class Meta:
#         model = Log
#         fields = ('job_request', 'atomic_job_id', 'stdout', 'stderr')
