# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from datetime import datetime
import logging
import uuid

#from django_celery_results.models import TaskResult
from django.db import models
from jsonfield import JSONField
from django.conf import settings
from django_celery_results.models import TaskResult

from cloudts.api.data.models import Workspace

logger = logging.getLogger(__name__)


class JobRequest(models.Model):
    backend_task = models.ForeignKey(TaskResult,
                                     null=True,
                                     on_delete=models.SET_NULL,
                                     related_name='job_request')
    # TODO: on delete should keep the workspace. Consider *never* deleting a
    # workspace but setting it disabled. A good implementation for this would be
    # https://django-model-utils.readthedocs.io/en/latest/models.html#softdeletablemodel
    # TODO: also consider that this model should be a soft deletable object?
    workspace = models.ForeignKey(Workspace,
                                  related_name='jobs',
                                  on_delete=models.SET_NULL,
                                  null=True)
    query = models.TextField('Query', blank=False, null=False)
    content = JSONField('Processing sequence', null=False)
    stage_jobs = JSONField('Stage jobs', null=False, default=[])
    prev_stage = models.ForeignKey('self',
                                   null=True,
                                   related_name='%(class)s_prev_stage',
                                   on_delete=models.CASCADE)
    next_stage = models.ForeignKey('self',
                                   null=True,
                                   related_name='%(class)s_next_stage',
                                   on_delete=models.CASCADE)
    # log_directory = models.CharField('Log directory URL', blank=True,
    #                                  null=False, default='', max_length=1024)

    @property
    def stage_id(self):
        if self.prev_stage is not None:
            return self.prev_stage.stage_id + 1
        else:
            return 0

    @property
    def backend_id(self):
        if self.backend_task is None:
            job_id = 'no-id'
        else:
            job_id = self.backend_task.task_id
        return 'cloudts-job-{user}-{job_id}-{stage_id}'\
            .format(user=self.workspace.owner,
                    job_id=job_id,
                    stage_id=self.stage_id)

    def __str__(self):
        return 'JobRequest {}'.format(self.backend_id)


# # Logs: commented (this still needs work)
#
# class Log(models.Model):
#     parent_job = models.ForeignKey(JobRequest,
#                                    null=False,
#                                    on_delete=models.CASCADE,
#                                    related_name='logs')
#     atomic_job_id = models.CharField(max_length=64)
#     stdout = models.TextField('Standard output', blank=True, null=False)
#     stderr = models.TextField('Standard error', blank=True, null=False)
#


class JobConfiguration(models.Model):
    """ User-dependent configuration for job backends """

    user = models.OneToOneField(to=settings.AUTH_USER_MODEL,
                                related_name='job_config',
                                on_delete=models.CASCADE)

    # Config is a JSON, because the structure of what we want to configure
    # is not really clear right now. Also, it may depend of the underlying
    # backend (right now it's just dataproc)
    config = JSONField(verbose_name='Unstructured (json) configuration',
                       null=False)

    def __str__(self):
        return 'JobConfiguration (user {user}, keys: {keys})'.format(
            user=self.user.get_username(),
            keys=', '.join(self.config.keys())
        )