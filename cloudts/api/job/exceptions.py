# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


from rest_framework.exceptions import APIException


# TODO: consider a custom common exception, not this one because this is related
# to Django REST Framework
class JobAPIException(APIException):
    """Base exception for CloudTS Job API

    Intended to be used when we want to inform the consumer that something went
    wrong. The easiest way to plug this in Django Rest Framework would be to
    follow the documentation in
    http://www.django-rest-framework.org/api-guide/exceptions/#custom-exception-handling

    To specify a message, use the `detail` keyword parameter in the
    constructor.

    """
    def __init__(self, detail=None, code=None, status_code=None):
        super().__init__(detail, code)
        if status_code is not None:
            self.status_code = status_code


class RestAPIException(JobAPIException):
    """Exception for problems when communicating with the rest APIs

    As described in [RES-214], communication to rest APIs have sometimes some
    errors, due to a hiccup on the network or some situation out of our control.
    This exception is intended for those cases so that the code can try again
    some time later
    """
    pass


class ConfigurationException(Exception):
    """ Exception for configuration problems.

    This is not an APIException since it is only an internal exception
    """
    pass
