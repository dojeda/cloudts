# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import json
import logging
import random
import string
import urllib
from datetime import timedelta

import pandas as pd
import celery
import django.db.utils
import google.auth.exceptions
from celery.exceptions import Ignore
from django.conf import settings
from django.utils.timezone import now

from cloudts.api.data.backends import get_backend as get_data_backend
from cloudts.api.data.models import Workspace
from cloudts.api.job.backends import get_backend as get_job_backend
from cloudts.api.job.backends.ncvt import get_jobs
from cloudts.api.job.exceptions import RestAPIException
from cloudts.api.job.models import JobRequest

logger = logging.getLogger(__name__)


class BaseTask(celery.Task):
    """ A base task that logs when it finishes successfully or with failure """

    def on_success(self, retval, task_id, args, kwargs):
        logger.info('Task %s finished successfully', task_id)
        return super().on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.warning('Task %s raised exception: type: %s repr:\n%r',
                       task_id, type(exc), exc, exc_info=einfo)
        return super().on_failure(exc, task_id, args, kwargs, einfo)

    def apply_async(self, args=None, kwargs=None, task_id=None, producer=None,
                    link=None, link_error=None, shadow=None, **options):
        # Force new tasks to be immediately saved in the TaskResult database.
        # logger.debug('apply_async with args=%s, kwargs=%s, task_id=%s, producer=%s '
        #             'link=%s, link_error=%s, shadow=%s, options=%s, (ignore_result=%s)',
        #             args, kwargs, task_id, producer, link, link_error, shadow, options,
        #             self.ignore_result)
        async_result = super().apply_async(args, kwargs, task_id, producer,
                                           link, link_error, shadow, **options)
        if not self.ignore_result:
            self.update_state(task_id=async_result.id, state='PENDING')
        return async_result

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        if exc is not None:
            logger.warning('Task %s will be retried due to exception %s', task_id, type(exc))
        else:
            logger.info('Task %s will be retried by explicit request', task_id)
        if not self.ignore_result:
            self.update_state(task_id, state='WAITING')
        return super().on_retry(exc, task_id, args, kwargs, einfo)


# Override decorators of celery.shared_task
def shared_task(*args, **kwargs):
    """Task decorator for robust tasks

    Robust tasks are tasks that will be retried 5 times if one of the
    exceptions in ...
    """
    kwargs['base'] = kwargs.get('base', None) or BaseTask
    # [RES-216]
    # Add the django.db.utils.OperationalError in the auto retry list of
    # exceptions. It seems that postgres is closing some connections and this
    # makes the task fail
    kwargs['autoretry_for'] = kwargs.get('autoretry_for', ()) + (
        django.db.utils.OperationalError,
        google.auth.exceptions.RefreshError,   # [RES-214]
    )
    if 'max_retries' not in kwargs:
        kwargs['max_retries'] = 5
    if 'countdown' not in kwargs and 'eta' not in kwargs:
        # TODO: the countdown might not be working on celery 4.1.0.
        # See https://github.com/celery/celery/issues/4221
        # TODO: should this be in the 'retry_kwargs' dictionary ? Maybe not.
        # http://docs.celeryproject.org/en/latest/userguide/tasks.html#Task.retry_kwargs
        kwargs['countdown'] = 10
    return celery.shared_task(*args, **kwargs)


def shared_monitor_task(*args, **kwargs):
    kwargs['max_retries'] = None  # retry forever
    return shared_task(*args, **kwargs)


@shared_task
def debug_task(self):
    logger.info('cloudts.api.jobs.tasks.debug_task is running')
    import time;
    time.sleep(15)
    return 'Result debug'


@shared_task
def workspace_scan(workspace_pk):
    logger.info('Creating views for workspace {}'.format(workspace_pk))

    try:
        instance = Workspace.objects.get(pk=workspace_pk)
    except Workspace.DoesNotExist:
        logger.error('Workspace {} not found'.format(workspace_pk))
        raise
    # Retrieve the backend and workspace object and call scan, which is the slow
    # task here
    backend = get_data_backend()
    ws_object = backend.retrieve_workspace(model=instance)
    ws_object.scan()


@shared_task
def workspace_commit(workspace_pk):
    logger.info('Committing workspace {}'.format(workspace_pk))

    try:
        instance = Workspace.objects.get(pk=workspace_pk)

    except Workspace.DoesNotExist:
        logger.error('Workspace {} not found'.format(workspace_pk))
        raise

    # Retrive the backend and workspace object and call commit, which is the
    # slow task here
    backend = get_data_backend()
    backend.commit(model=instance)


@shared_task
def metadata_batch_update(workspace_pk, batch_metadata):
    logger.info('Batch updating %s with %d elements', workspace_pk, len(batch_metadata))

    try:
        workspace = Workspace.objects.get(pk=workspace_pk)

    except Workspace.DoesNotExist:
        logger.error('Workspace {} not found'.format(workspace_pk))
        raise

    backend = get_data_backend()
    ws_object = backend.retrieve_workspace(model=workspace)

    # Update each file at a time
    # output_metadata = []
    workspace_changed = False
    for file_metadata in batch_metadata:
        try:
            file_id = file_metadata['id']
            new_metadata = file_metadata['metadata']
            for family in new_metadata:
                new_metadata[family]['id'] = file_id
                ws_object.update_metadata(file_id, family, new_metadata[family],
                                          owner=workspace.owner.get_username(),
                                          permissions=0o600)

            # metadata_result = ws_object.get_metadata(file_id)
            # known_families = set(new_metadata)
            # known_families.add('cloudts')
            # # Only respond with the metadata families that were modified in
            # # this update and the cloudts family
            # metadata_result = metadata_result.filter(family__in=known_families)
            # output_metadata.append(metadata_result)

            # Update the workspace definition if there were new families included
            for family in new_metadata:
                if family not in workspace.families:
                    workspace.families[family] = 0
                    workspace_changed = True
        except:
            logger.error('Error updating metadata of file %s', file_id, exc_info=True)
            raise

    # Save the workspace object if there were new families included
    if workspace_changed:
        workspace.save()


@shared_task(bind=True)
def schedule_sequence(self, job_request_pk):
    logger.info('Scheduling a processing sequence...')
    # TODO: add workspace permission check?
    job_backend = get_job_backend()
    data_backend = get_data_backend()
    job_request = JobRequest.objects.get(pk=job_request_pk)
    # Check if the associated backend task is set. If it is None, it is very
    # likely that the worker code was executed before the rest api code had
    # the chance to save the JobRequest object (see JobViewSet.create)
    if job_request.backend_task is None:
        logger.warning('JobRequest %s does not have a backend task, '
                       'retrying in 10 seconds', job_request)
        raise self.retry(eta=now() + timedelta(seconds=10))

    # Query the workspace
    workspace = data_backend.retrieve_workspace(model=job_request.workspace)
    query_result = workspace.query(sql=job_request.query)
    dataframe = pd.DataFrame.from_records(query_result)

    # [RES-261]
    # the database can be empty and this should not produce an error
    if dataframe.empty:
        logger.info('Query is empty: nothing to do here')
        return

    # Prepare and verify the NCVT configuration
    if 'url' not in dataframe.columns or 'id' not in dataframe.columns:
        # TODO: design/create a exception that is celery-compatible
        raise Exception('Queries must have a "url" and "id" column')
    ncvt_config = {
        'data_path': data_backend.data_uri,
        'data_temp_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'data_temp'),
        'script_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'scripts'),
        'results_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'results'),
        'index_column': 'id',
        'filename_column': 'url',
        'csv_separator': ',',
        'enforce_order': True,
        'local': False,
        'job_prefix': 'cloudts_job_{}'.format(job_request.stage_id),
        'shuffle_database': False,  # [RES-265]: do not shuffle the database by default
    }

    # Ask NCVT to generate as much jobs as it can
    logger.debug('Sequence is\n%s', json.dumps(job_request.content, indent=2))
    jobs_by_stage, remaining_sequence = get_jobs(job_request.content,
                                                 dataframe,
                                                 ncvt_config)
    logger.info('NCVT scheduler gave a plan of %s stages and %d total jobs',
                len(jobs_by_stage), sum(len(_x) for _x in jobs_by_stage))

    # Verify if the last job is a metadata collect job or a PySpark job, which
    # are the special cases for the job API
    #
    # Remove the metadata collect job: we don't want the backend to execute it
    # because it should be a Celery task who handles that
    metadata_collect_job, backend_jobs = _extract_metadata_collect(jobs_by_stage)

    # If there is a metadata collect job, we need to determine what the next
    # query is
    if metadata_collect_job:
        logger.info('Plan has a metadata collect job, estimating next query')
        next_query = _estimate_next_query(metadata_collect_job, job_request.query)
        logger.info('Next query is:\n%s', next_query)
    else:
        logger.info('Next stage has the same query: no metadata is modified')
        next_query = job_request.query

    # Special case: PySpark jobs are a single job per stage and need to be
    # scheduled separately
    # TODO: refactor this check on a function
    if (len(jobs_by_stage) == 1 and  # There is only one stage
            len(jobs_by_stage[0]) == 1 and  # There is only one job on the stage
            jobs_by_stage[0][0][1]['Interpreter'] == 'PySpark'):

        logger.info('A PySpark job!')
        job_id = job_backend.submit_pyspark(job_instance=job_request,
                                            workspace_instance=workspace,
                                            job=jobs_by_stage[0][0])

    else:
        # Send the job list to the backend, which will send it to the Spark cluster
        # TODO: a good try/catch here?
        project_id =  settings.CLOUDTS_CONFIG['job_backend']['options']['project_name']
        job_id = job_backend.submit(job_instance=job_request,
                                    workspace_instance=workspace,
                                    job_list=backend_jobs,
                                    query=job_request.query,
                                    dataframe=dataframe,
                                    project_id=project_id)

    # Update the job request object.
    # If there is some remaining sequence, create it and associate it to the
    # job request
    job_request.stage_jobs = jobs_by_stage
    if remaining_sequence:
        logger.info('Preparing for next sequence. The next query is:\n%s', next_query)
        next_stage = JobRequest.objects.create(
            backend_task=job_request.backend_task,
            workspace=job_request.workspace,
            query=next_query,
            content=remaining_sequence,
            stage_jobs=[],
            prev_stage=job_request,
            next_stage=None,
        )
        next_stage.save()
        job_request.next_stage = next_stage
    else:
        logger.info('There is no next sequence, the NCVT plan is complete')
        job_request.next_stage = None
    job_request.save()

    logger.info('Replacing job %s with a waiting backend job', self.request.id)
    wait_for_backend_sig = wait_for_backend.signature(
        # Note: use backend_jobs so that the metadata collect is considered!
        # (workspace_pk, job_id, stage_jobs, remaining_sequence)
        (job_request.pk,)
    )
    raise self.replace(wait_for_backend_sig)


def _estimate_next_query(collect_job, previous_query):
    # Collect a dictionary of {family: [list of new elements]}
    new_metadata = {}
    for metadata_operation in collect_job['Metadata']:
        for name in metadata_operation:
            if 'Family' not in metadata_operation[name]:
                raise Exception('New metadata must have a Family')
            family = metadata_operation[name]['Family']
            if family not in new_metadata:
                new_metadata[family] = {'id'}  # For the set to have the minimal "id" column. Update: apparently, this creates an ambiguous query
            new_metadata[family].add(name)

    # Convert columns to aliases columns if needed
    aliased_ids = dict()
    for family in new_metadata:
        aliased_columns = []
        for column in new_metadata[family]:
            if column == 'id':
                alias = '_aliased_id_{0}'.format(''.join(random.choice(string.ascii_letters) for _ in range(6)))
                aliased_ids[family] = alias
                aliased_columns.append('"{0}"."id" AS "{1}"'.format(family, alias))
            else:
                aliased_columns.append('"{0}"."{1}"'.format(family, column))
        new_metadata[family] = aliased_columns

    # Create a new SELECT new_elements FROM family
    new_selects = [
        (
            family,
            'SELECT {columns} FROM {table}'.format(table=family, columns=', '.join(aliased_columns))
        )
        for family, columns in new_metadata.items()
    ]

    # Join all new selects with the previous query
    new_query = ['SELECT * FROM (', previous_query, ') t1']
    for i, (family, statement) in enumerate(new_selects, start=2):
        table_name = 't{}'.format(i)
        id_column = aliased_ids[family]
        new_query.extend(['LEFT JOIN (', statement, ')', table_name,
                          'ON (t1.id = "{0}"."{1}")'.format(table_name, id_column)])
    return '\n'.join(new_query)




@shared_task(bind=True)
def scan_and_reschedule(self, job_request_pk):
    logger.info('Scanning and rescheduling...')
    job_request = JobRequest.objects.get(pk=job_request_pk)
    workspace_scan(job_request.workspace.pk)

    if job_request.next_stage is not None:
        logger.info('Replacing job %s with a schedule sequence (for the next stage)',
                    self.request.id)
        schedule_sequence_sig = schedule_sequence.signature(
            (job_request.next_stage.pk,)
        )
        raise self.replace(schedule_sequence_sig)

    logger.info('Job finished, no more jobs to schedule')
    return


@shared_task(bind=True)
def metadata_collect(self, job_request_pk):
    logger.info('Metadata collect job!')
    job_request = JobRequest.objects.get(pk=job_request_pk)
    data_backend = get_data_backend()
    workspace_instance = data_backend.retrieve_workspace(model=job_request.workspace)

    # Extract the metadata collect job
    collect_job, _ = _extract_metadata_collect(job_request.stage_jobs)

    # Collect all metadata
    logger.info('metadata collect has %d operations', len(collect_job))
    new_metadata = {}
    for metadata_operation in collect_job['Metadata']:
        for name, operation in metadata_operation.items():
            op_type = operation['Type']
            index = operation['Index']
            family = operation['Family']  # TODO: verify that the family is present
            if index not in new_metadata:
                new_metadata[index] = {}
            if family not in new_metadata[index]:
                new_metadata[index][family] = {'id': index}

            if op_type == 'contents':
                url = operation['Value']
                if not isinstance(url, str) or not url.startswith('gs://'):
                    raise Exception('Invalid metadata value {} for a "contents" operation'
                                    .format(value))
                value = _read_file(url, data_backend)
            elif op_type == 'filename':
                # We need to find the file on the workspace or the global workspace
                # whose URL is the one in the filename OR if whose hash and size
                # match (because the URL might be reused)
                # IDEA:
                # It would be even better to have a list of filename:id or
                # filename:(id, url) from the previous file collect job
                raise NotImplementedError('Metadata filename operation disabled')
                # To add:
                # if not isinstance(url, str) or not url.startswith('gs://'):
                #     raise Exception('Invalid metadata value {} for a "filename" operation'
                #                     .format(value))
            elif op_type in 'value':
                value = operation['Value']
            else:
                raise Exception('Invalid metadata operation {}'.format(op_type))

            new_metadata[index][family][name] = value

    # Send the new metadata to the Data API
    for file_handler, file_metadata in new_metadata.items():
        for family, family_metadata in file_metadata.items():
            logger.info('Updating metadata of file %s for family %s',
                        file_handler, family)
            workspace_instance.update_metadata(file_handler, family,
                                               family_metadata,
                                               owner=job_request.workspace.owner.get_username(),
                                               permissions=0o600)

    # Request a metadata scan followed by a schedule sequence
    logger.info('Replacing job %s with scanning the database', self.request.id)
    scan_and_reschedule_sig = scan_and_reschedule.signature(
        (job_request.pk,)
    )
    raise self.replace(scan_and_reschedule_sig)


def _extract_metadata_collect(stages):
    last_stage = stages[-1]
    if len(last_stage) == 1:
        # MetadataCollect jobs are the only contents of a stage
        job_id, job_contents = last_stage[0]
        if job_contents['Name'] == 'MetadataCollect':
            return job_contents, stages[:-1]
    return None, stages


@shared_task(bind=True)
def collect_files(self, job_request_pk):  # workspace_pk, jobs, sequence):
    logger.info('collecting files...')
    # TODO: add workspace permission check?
    job_request = JobRequest.objects.get(pk=job_request_pk)
    logger.info('Celery task is %s', job_request.backend_task)
    # logger.info('jobs are:\n{}'.format(json.dumps(jobs), indent=2))
    data_backend = get_data_backend()
    workspace_instance = data_backend.retrieve_workspace(model=job_request.workspace)

    # Detect and pop the metadata collect job
    metadata_collect_job, jobs = _extract_metadata_collect(job_request.stage_jobs)

    # Collect all files
    files_collected = {}
    errors = []
    data_path = data_backend.data_uri,
    data_temp_path = '{}/{}'.format(workspace_instance.data_uri.rstrip('/'), 'data_temp')
    results_path = '{}/{}'.format(workspace_instance.data_uri.rstrip('/'), 'results')
    for stage in jobs:
        for _, atomic_job in stage:
            job_outputs = atomic_job.get('Outputs', {})
            for output_name, output_file in job_outputs.items():
                if output_file in files_collected:
                    continue
                if output_file.endswith('.parquet'):
                    logger.info('Special case: ignore parquet files (%s)', output_file)
                    continue
                if output_file.startswith(data_temp_path):
                    #logger.info('Special case: temp file are not added (%s)', output_file)
                    continue
                    # ncvt_metadata = {'temporal': True}
                    #filename = output_file.replace(data_temp_path, '', 1).lstrip('/')
                    # TODO: extract path and put it on the cloudts family?
                elif output_file.startswith(results_path):
                    ncvt_metadata = {'temporal': False}
                    filename = output_file.replace(results_path, '', 1).lstrip('/')
                else:
                    # this is an error but let the loop continue so that we can
                    # send files as long as possible
                    errors.append('Cannot add file ({}) outside the results or temp directories'
                                  .format(output_file))
                    continue

                # Add the file to CloudTS
                logger.info('Adding %s', output_file)
                file_url, basic_metadata = workspace_instance.add_file(output_file,
                                                                       owner=job_request.workspace.owner.get_username(),
                                                                       permissions=0o600)
                # Update its metadata
                # TODO: inherit metadata
                ncvt_metadata['id'] = basic_metadata.file_handler
                workspace_instance.update_metadata(basic_metadata.file_handler,
                                                   'ncvt', ncvt_metadata,
                                                   owner=job_request.workspace.owner.get_username(),
                                                   permissions=0o600)
                files_collected[output_file] = file_url

    if errors:
        raise Exception('Could not upload all files: {}'
                        .format('\n'.join(errors)))
    logger.info('Added files:\n%s', '\n'.join('{} -> {}'.format(k, v)
                                              for k, v in files_collected.items()))

    # After collecting files, check if there is more jobs to do.

    # Case 1: there was a metadata collect job
    if metadata_collect_job is not None:
        logger.info('Replacing job %s with a collect metadata job',
                    self.request.id)
        metadata_collect_sig = metadata_collect.signature(
            (job_request.pk,))
        raise self.replace(metadata_collect_sig)

    # Case 2: there is  still more processing sequences to run
    if job_request.next_stage is not None:
        logger.info('Replacing job %s with a schedule sequence (for the next stage)',
                    self.request.id)
        schedule_sequence_sig = schedule_sequence.signature(
            (job_request.next_stage.pk,)
        )
        raise self.replace(schedule_sequence_sig)

    logger.info('Job finished, no more jobs to schedule')
    return


@shared_monitor_task(bind=True)
def wait_for_backend(self, job_request_pk):  # (self, workspace_pk, backend_job_id, jobs, sequence):
    # TODO: add workspace permission check?
    logger.info('Waiting for job %s on the backend...', job_request_pk)
    job_request = JobRequest.objects.get(pk=job_request_pk)
    job_backend = get_job_backend()
    logger.info('Celery task is %s', job_request.backend_task)
    user = job_request.workspace.owner

    try:
        updated_status, details_object = job_backend.status(job_id=job_request.backend_id, user=user)
    except RestAPIException:
        # Some network problem has occurred, we need to try again later
        logger.warning('Status request failed, trying again in 10 seconds...')
        raise self.retry(eta=now() + timedelta(seconds=10))

    # When the task failed in the backend we can extract some details
    if updated_status in ('CANCELLED', 'ERROR'):
        logger.info('Backend job %s has finished with a failure',
                    job_request.backend_id)
        details = 'Job failed on backend. Details: {}'.format(details_object)
        # TODO: at least collect the files that did not fail!
        # Mark this task as failed and ignore it so it does not get re-scheduled
        self.backend.mark_as_failure(self.request.id,
                                     Exception(details))
        raise Ignore

    elif updated_status in ('DONE',):
        logger.info('Backend job %s has finished successfully',
                    job_request.backend_id)
        # TODO: manage the next step of this task
        collect_files_sig = collect_files.signature((job_request_pk,))
        raise self.replace(collect_files_sig)

    logger.info('Backend job %s still not finished. Will retry later',
                job_request.backend_id)

    # No results are available, so let's try again later.
    #
    # TODO: find a way for celery to retry this task AND also set the status
    # to something different than "RETRY", as this state name is
    # confusing. The only way I can think of doing this to create our own
    # result backend that extends DataBaseBackend and bypasses the retry
    # state on the _store_result
    raise self.retry(eta=now() + timedelta(seconds=10))  # TODO: parametrize this time


def _read_file(url, backend_obj):
    parsed_url = urllib.parse.urlparse(url)
    bucket_name = parsed_url.netloc
    blob_name = parsed_url.path.lstrip('/')
    client = backend_obj.client
    bucket = client.get_bucket(bucket_name)
    blob = bucket.get_blob(blob_name)
    if blob is None:
        raise Exception('Blob on {} does not exist'.format(url))
    return blob.download_as_string().decode('utf-8')