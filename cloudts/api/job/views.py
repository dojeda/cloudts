# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import logging
import uuid
from datetime import timedelta

from django.utils.timezone import now
from django_celery_results.models import TaskResult
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.reverse import reverse

from cloudts.api.job.serializers import (
    JobSerializer, JobCreateSerializer, JobDetailSerializer
)
from cloudts.api.job.tasks import schedule_sequence


logger = logging.getLogger(__name__)


class JobViewSet(mixins.ListModelMixin,
                 mixins.RetrieveModelMixin,
                 mixins.CreateModelMixin,
                 viewsets.GenericViewSet):

    queryset = TaskResult.objects.all()
    serializer_class = JobSerializer
    lookup_field = 'task_id'

    def get_serializer_class(self):
        # For retrieve (GET jobs/id/), we want to give all the details of
        # the workspace. For all other actions, we only give a small
        # representation.
        # For job creation (POST jobs/), we want a specific serializer that has
        # the processing sequence
        if self.action == 'create':
            return JobCreateSerializer
        return JobDetailSerializer

    def create(self, request, *args, **kwargs):

        # TODO: verify permissions!!!
        # TODO: consider moving the job api inside a workspace

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        job_request = serializer.validated_data
        # TODO: This code... is it necessary ?
        # # the create() of the serializer will now be passed an 'owner' field
        # try:
            #job_request = serializer.save(owner=self.request.user)
            # job_id = str(uuid.uuid4())
            # job_request = serializer.save()
            # except JobAPIException as ex:
            #     # Propagate the JobAPIException
            #     raise
        # except:
        #     logger.error('Could not create Job', exc_info=True)
        #     raise JobAPIException(detail='Unexpected error',
        #                         code='error',
        #                         status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        job_id = str(uuid.uuid4())
        job_request = serializer.save()
        task = schedule_sequence.apply_async(task_id=job_id,
                                             kwargs=dict(job_request_pk=job_request.pk),
                                             eta=now() + timedelta(seconds=10))
        task_result = TaskResult.objects.get_task(job_id)
        logger.info('TaskResult gave %s %s %s', task_result, task_result.pk, task_result._state.adding)
        # TODO: there is a weird bug here where sometimes, the get_task gives None
        # We need to find a workaround for that
        # Update:
        # I believe what happens is that the worker code runs before the next
        # two lines are executed (which updates the database)
        job_request.backend_task = task_result
        job_request.save()
        logger.info('JobRequest gave %s %s %s', job_request, job_request.backend_task, job_request._state.adding)
        #task_result.save()

        # I don't think we need this, and it creates a problem because the
        # worker can modify the model before this happens
        #task_result.save()

        headers = {
            'Location': reverse('job-detail',
                                kwargs=dict(task_id=task_result.task_id),
                                request=request),
        }
        output_serializer = JobSerializer(task_result)
        return Response(output_serializer.data,
                        headers=headers,
                        status=status.HTTP_201_CREATED)

        # job_backend = get_backend()
        # data_backend = get_data_backend()
        # try:
        #     workspace = data_backend.retrieve_workspace(model=instance.workspace)
        #     query_result = workspace.query(sql=instance.query)
        # except:
        #     logger.warning('Workspace query failed', exc_info=True)
        #     raise JobAPIException(detail='Workspace query failed',
        #                           code='error',
        #                           status_code=status.HTTP_400_BAD_REQUEST)

        # database = pd.DataFrame.from_records(query_result)
        # if 'url' not in database.columns:
        #     raise JobAPIException(detail='Query must have a \'url\' column',
        #                           code='error',
        #                           status_code=status.HTTP_400_BAD_REQUEST)

        # # TODO: create the real list of json files. For the moments, let us
        # # assume that each element of the ProcessingSequence is a job.
        # #all_jobs = instance.content['ProcessingSequence']
        # #tmpdir = tempfile.TemporaryDirectory()
        # #database = None
        # cloudts_config = {
        #     'data_path': data_backend.data_uri,
        #     'data_temp_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'data_temp'),
        #     #'script_path': '{}/{}-{}'.format(workspace.data_uri.rstrip('/'), 'scripts', instance.id),
        #     'script_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'scripts'),
        #     #'results_path': '{}/{}-{}'.format(workspace.data_uri.rstrip('/'), 'results', instance.id),
        #     'results_path': '{}/{}'.format(workspace.data_uri.rstrip('/'), 'results'),
        #     'index_column': 'url',
        #     'csv_separator': ',',
        #     # 'google_cloud': {
        #     #     'service_account': settings.CLOUDTS_CONFIG['job_backend']['options']['json_credentials']
        #     # }
        # }
        # all_jobs = get_jobs(instance.content, database, cloudts_config)

        # logger.info('NCVT scheduler gave a plan of {} stages and {} total jobs'
        #             .format(len(all_jobs), sum(len(_x) for _x in all_jobs)))
        # #try:
        # #backend = get_backend()

        # # TODO: a good try/catch here
        # print('all_jobs', all_jobs, end='\n')
        # job_id = job_backend.submit(job_instance=instance,
        #                             workspace_instance=workspace,
        #                             job_list=all_jobs)

        # return job_id

        # except:
        #     logger.error('Could not submit job')
        #     raise JobAPIException(detail='Unexpected error',
        #                           code='error',
        #                           status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


#     # TODO: find out why the router does not find this method in the browsable
#     # root API
#     @detail_route(methods=['post'], url_path='cancel')
#     def cancel(self, request, pk):

#         # cancel job
#         try:
#             backend = get_backend()
#             current_job = Job.objects.get(id=pk)
#             backend.cancel(job_id=current_job.backend_id)
#         except:
#             logger.error('Could not cancel job {}'.format(pk))
#             raise JobAPIException(detail='Unexpected error',
#                                    code='error',
#                                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

#         logging.info('Canceled job {}'.format(pk))

#         # update status
#         try:
#             current_job.status = backend.status(job_id=current_job.backend_id)
#             current_job.save()

#         except:
#             logger.error('Could not update status of canceled job {}'.format(pk))
#             raise JobAPIException(detail='Unexpected error',
#                                     code='error',
#                                     status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

#         return Response(data={}, status=status.HTTP_200_OK)

#     # TO DO: Create automatic routine which updates all status's regularly
#     # Now when we do a GET we update status at the same time
#     def retrieve(self, request, pk):
#         """ retrieve job details """

#         try:
#             backend = get_backend()
#             #current_job = Job.objects.get(id=pk)
#             current_job = self.get_object()
#             current_job.status = backend.status(job_id=current_job.backend_id)
#             current_job.save()

#         except:
#             logger.error('Could not update status of retrieve job {}'.format(pk))
#             raise JobAPIException(detail='Unexpected error',
#                                     code='error',
#                                     status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

#         serializer = self.get_serializer(current_job)
#         return Response(serializer.data)

# Logs: commented; this still needs work
# class LogViewSet(mixins.ListModelMixin,
#                  mixins.RetrieveModelMixin,
#                  viewsets.GenericViewSet):
#
#     queryset = Log.objects.all()
#     serializer_class = LogSerializer
#     #serializer_class = JobSerializer
#     #lookup_field = 'atomic_job_id'
#
#     def retrieve(self, request, *args, **kwargs):
#         # We need a custom code here because the lookup is not trivial [is it?]
#         # instance = self.get_object()
#         # serializer = self.get_serializer(instance)
#         # return Response(serializer.data)
#         return Response(data={'message':'not implemented yet'})
