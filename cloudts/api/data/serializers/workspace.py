# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""DRF serializers for workspace operations"""

from rest_framework import serializers

from cloudts.api.data.models import Workspace
from cloudts.api.data.serializers.base import JSONSerializerField


class WorkspaceSerializer(serializers.ModelSerializer):
    """ Serializer for workspace retrieve, list and create """

    owner = serializers.SerializerMethodField()
    families = JSONSerializerField(required=False)
    data_url = serializers.SerializerMethodField()
    url = serializers.HyperlinkedIdentityField(view_name='workspace-detail')

    def get_owner(self, obj): # pylint: disable=no-self-use,missing-docstring
        return obj.owner.username

    def get_data_url(self, obj): # pylint: disable=no-self-use,missing-docstring
        return obj.get_data_url()

    class Meta:
        model = Workspace
        fields = ('name', 'owner', 'description', 'date_created', 'families',
                  'data_url', 'url')
        extra_kwargs = {
            'owner': {'read_only': True},
            'date_created': {'read_only': True},
            'data_url': {'read_only': True},
        }


class QuerySerializer(serializers.Serializer):
    """ Serializer for a SQL query on a workspace """
    sql = serializers.CharField(trim_whitespace=True, allow_blank=False)
    fast = serializers.BooleanField(required=False, default=None)

    def update(self, instance, validated_data):
        raise NotImplementedError('%s does not update'%self.__class__.__name__)

    def create(self, validated_data):
        raise NotImplementedError('%s does not create'%self.__class__.__name__)
