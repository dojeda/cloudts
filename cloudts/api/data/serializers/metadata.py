# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""DRF serializers for file and metadata operations"""

from collections import Mapping, OrderedDict

from django.db.models.query import QuerySet
from rest_framework import serializers

from cloudts.api.data.models import Metadata
from cloudts.api.data.serializers.base import BasicSerializer, JSONSerializerField


class FileCreateSerializer(BasicSerializer):
    """Serializer for file create request that use a URL"""
    url = serializers.CharField(required=False)


class MetadataSerializer(BasicSerializer):
    """Serializer for file detail response"""
    metadata = serializers.SerializerMethodField()

    def get_metadata(self, obj): # pylint: disable=missing-docstring
        # It's either a queryset that has many metadata
        if isinstance(obj, QuerySet):
            merged_metadata = {}
            for entry in obj:
                merged_metadata.update(entry.get_metadata_dict())
            return merged_metadata
        # Or it's a simple object
        return obj.get_metadata_dict()

    class Meta:
        model = Metadata
        fields = ('id', 'url', 'workspace', 'metadata')


class MetadataUpdateSerializer(BasicSerializer):
    """Serializer for metadata update requests"""
    metadata = JSONSerializerField(required=True)

    def validate_metadata(self, value): # pylint: disable=no-self-use,missing-docstring
        if not isinstance(value, Mapping):
            raise serializers.ValidationError('Metadata must be a mapping (dict) '
                                              'of families to values')
        for family_name, metadata in value.items():
            if not isinstance(metadata, Mapping):
                raise serializers.ValidationError('Metadata of family "{}" is not '
                                                  'a mapping (dict) of families to values'
                                                  .format(family_name))
        return value


class MetadataBatchUpdateSerializer(MetadataUpdateSerializer):
    """Serializer for batch (several files) metadata modifications"""
    id = serializers.CharField(required=True)


class FileRecordQuerySerializer(BasicSerializer):
    """Serializer for querying existing files"""
    # File handler and url are used for the response
    file_handler = serializers.UUIDField(read_only=True)
    url = serializers.CharField(read_only=True)
    # hash and size are used in the request
    hash = serializers.CharField(allow_blank=False)
    size = serializers.IntegerField()


class QueryResultSerializer(BasicSerializer):
    """A general serializer for workspace query responses"""

    def to_representation(self, instance):
        # The representation is the same object, but we will force the id to be
        # first every time, if present
        odict = OrderedDict()
        if 'id' in instance:
            odict['id'] = instance['id']
        odict.update(instance)
        return odict
