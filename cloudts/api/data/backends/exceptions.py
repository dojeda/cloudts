# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

"""
Data API exceptions
"""


class DataBackendException(Exception):
    """ Base exception to cloudts data backend errors """


class ResourceAlreadyExistsException(DataBackendException):
    """ Exception thrown when a backend resource already exists """
    pass


class ConflictException(DataBackendException):
    """ Exception thrown when an operation cannot be performed due to a conflict """
    pass


class FileNotFoundException(DataBackendException):
    """ Exception thrown when a file does not exist """
    pass
