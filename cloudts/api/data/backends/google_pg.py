# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import base64
import binascii
import collections
import datetime
import logging
import re
import tempfile

import os.path as osp

from django.contrib.postgres.aggregates import ArrayAgg
from django.db import connection, connections, transaction
from django.db.models import Q, Max, F
from google.cloud.storage import Bucket, Blob
from google.cloud.exceptions import NotFound, Conflict

from cloudts.client.google import RobustGoogleStorageClient
from cloudts.api.data.models import FileRecord, Metadata
from cloudts.api.data.backends.exceptions import ConflictException

from .base import AbstractBackend, AbstractDataWorkspace, AbstractMetadataWorkspace
from .exceptions import ResourceAlreadyExistsException, FileNotFoundException
from .utils import get_readable_info

logger = logging.getLogger(__name__)


# def _get_class(name):
#     module_name, class_name = name.split('.')
#     module = importlib.import_module(module_name)
#     return getattr(module, class_name)

# TODO: this needs to be renamed without the JSON part; this has nothing to do with that!

class GoogleJSONBackend(AbstractBackend):
    """ A backend that uses Google buckets and a postgres database.

    The data is saved as objects on Google buckets.
    The metadata is saved as a jsonb column in a Postgres database

    """

    def __init__(self, *, bucket_name, json_credentials, region, prefix,
                 readonly_user, enforce_permissions=True):
        super().__init__()
        # DataBackend initialization
        if not re.match('^[a-zA-Z_]+[a-zA-Z0-9_]*$', readonly_user):
            # Make sure that this name is not injectable
            raise ValueError('Invalid readonly username')
        self.__ro_user = readonly_user

        # Initialize Google API client
        logging.info('Attempting to use json credentials %s', json_credentials)
        self.client = RobustGoogleStorageClient.from_service_account_json(json_credentials)
        self.bucket = self.client.get_bucket(bucket_name)
        self.region = region
        self.prefix = prefix

        # Remote base directory
        self.remote_basedir = 'gs://{}/'.format(bucket_name)

    def permissions_enabled(self):
        """ Whether the backend instance enforces permissions. """
        return True

    @property
    def readonly_user(self):
        return self.__ro_user

    @property
    def data_uri(self):
        """ Unique Resource Identifier for the location of public data """
        return self.remote_basedir

    @property
    def metadata_uri(self):
        """ Unique Resource Identifier for the location of public metadata """
        return None

    def new_user(self, username, **kwargs):
        """Register a new user on the backend

        Some backends need to create or initialize a user in order to enforce
        the permissions. This function performs the initialization of whatever
        resource needed to create a user.

        Parameters
        ----------
        username: str
            The username of the new user.

        Returns
        -------
        None

        """
        logger.info('Creating a new user with the {} backend '
                    'does not do anything'.format(self.__class__.__name__))

    def create_workspace(self, *, model):
        """Create a workspace and initialize its resources.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the creation of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        object
            An object instance that implements AbstractDataWorkspace and
            AbstractMetadataWorkspace.

        """
        workspace = GoogleJSONWorkspace(backend=self,
                                        workspace_model=model,
                                        prefix=self.prefix,
                                        enforce_permissions=self.permissions_enabled,
                                        initialize=True)
        GoogleJSONBackend._update_start_id(model)
        return workspace

    @staticmethod
    def _update_start_id(workspace_model):
        last_id = Metadata.objects\
                          .values('id')\
                          .filter(workspace=None)\
                          .order_by('id')\
                          .last()
        if last_id is None:
            last_id = 0
        else:
            last_id = last_id['id']
        workspace_model.start_id = last_id

    def retrieve_workspace(self, *, model):
        """Retrieve an existing workspace.

        This method should work like create_workspace, but without the
        initialization of the resources.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the retrieval of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        object
            An object instance that implements AbstractDataWorkspace and
            AbstractMetadataWorkspace.

        """
        return GoogleJSONWorkspace(backend=self,
                                   workspace_model=model,
                                   prefix=self.prefix,
                                   enforce_permissions=self.permissions_enabled,
                                   initialize=False)

    def delete_workspace(self, *, model):
        """ Delete all resources of an existing workspace.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the retrieval of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        None

        """
        workspace = self.retrieve_workspace(model=model)
        workspace.delete()

    def update_workspace(self, *, model):
        # Retrieve the workspace, even we do not use it directly. At least
        # this will fail if the workspace does not exist
        workspace = self.retrieve_workspace(model=model)

        table_name = Metadata.objects.model._meta.db_table
        with transaction.atomic(), connection.cursor() as cursor:
            # Lock the database so that no other modification can change this
            # See more on locks on:
            # https://www.postgresql.org/docs/9.4/static/explicit-locking.html
            # Chose "ROW EXCLUSIVE" because it blocks any transaction that
            # modifies data
            logger.info('Locking the database...')
            cursor.execute('LOCK TABLE {table_name} IN ROW EXCLUSIVE MODE'
                           .format(table_name=table_name))

            # Get the latest families and versions on the global workspace
            latest_global_versions = Metadata.objects \
                                             .filter(workspace__isnull=True) \
                                             .values('family') \
                                             .annotate(latest_version=Max('version'))
            global_family_versions = {}
            for latest in latest_global_versions:
                global_family_versions[latest['family']] = latest['latest_version']
            logger.info('global: %s', global_family_versions)

            # Update the family entries for all metadata added in this workspace.
            # Also update the workspace model.
            # We only support updating to the latest version; updating to a
            # specific version seems too complicated (specially when determining
            # the latest id of the global workspace)
            for family_name in model.families:
                if family_name not in global_family_versions:
                    latest_known_version = 0
                else:
                    latest_known_version = global_family_versions[family_name] + 1

                queryset = Metadata.objects.filter(workspace=model.pk,
                                                   family=family_name)
                n_changed = queryset.update(version=latest_known_version)
                logger.info('Updated %d metadata entries', n_changed)
                model.families[family_name] = latest_known_version
                model.dirty = True
            GoogleJSONBackend._update_start_id(model)
            model.save()
            logger.info('New workspace definition %s %s', model, model.families)

    def commit(self, *, model, **kwargs):
        """ Commit the data and metadata of a workspace.

        model: object
            An object with the attributes needed for the creation of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        None

        """

        table_name = Metadata.objects.model._meta.db_table
        with transaction.atomic(), connection.cursor() as cursor:
            # Lock the database so that no other modification can change this
            # See more on locks on:
            # https://www.postgresql.org/docs/9.4/static/explicit-locking.html
            # Chose "ROW EXCLUSIVE" because it blocks any transaction that
            # modifies data
            logger.info('Locking the database...')
            cursor.execute('LOCK TABLE {table_name} IN ROW EXCLUSIVE MODE'
                           .format(table_name=table_name))

            # 1. Get the latest families and versions on the global workspace
            latest_global_versions = Metadata.objects\
                                             .filter(workspace__isnull=True)\
                                             .values('family')\
                                             .annotate(latest_version=Max('version'))
            global_family_versions = {}
            for latest in latest_global_versions:
                global_family_versions[latest['family']] = latest['latest_version']
            logger.info('global: %s', global_family_versions)

            # 2. get the families and versions of the workspace
            workspace_family_versions = model.families
            logger.info('local: {}'.format(workspace_family_versions))

            # 3. get the list of files and metadata
            queryset = Metadata.objects\
                               .filter(workspace=model.pk)
            logger.info('Related files are {}'.format(queryset))

            # 4. Manage conflicts: abort if there are any
            # Quick implementation: check that workspace family versions are
            # equal to the global or zero if not present in global
            logger.info('Checking for conflicts...')
            for family, version in workspace_family_versions.items():
                global_version = global_family_versions.get(family, 0)
                if version == 0 and global_version != 0:
                    msg = ('Family {0} was declared as new (version 0) '
                           'in workspace {1} but there is already a committed '
                           'version {2} for this family'
                           .format(family, model.name, global_version))
                elif version != 0 and version != global_version + 1:
                    msg = ('Family {0} on workspace {1} is at version {2} '
                           'but the latest committed version is {3};'
                           .format(family, model.name, version, global_version))
                    raise ConflictException(msg)

            # 5. copy files to global/public workspace
            # Not necessary, files are already in the global/public workspace,
            # but the permissions must be fixed

            # 6. udpate metadata references
            # Not necessary, files are already in the global/public workspace.

            # 7. drop useless metadata changes
            # TODO

            # 8. Set the version value to +1: (Update: No... It's already at +1)
            #    and set the workspace to null for this metadata to become global
            logger.info('Updating metadata')
            queryset.update(workspace=None) #version=F('version') + 1)

            # 9. Update the workspace families for future use!
            # 9.1 re-determine the max version for each family
            latest_global_versions = Metadata.objects\
                                             .filter(workspace__isnull=True)\
                                             .values('family')\
                                             .annotate(latest_version=Max('version'))
            global_family_versions = {}
            for latest in latest_global_versions:
                global_family_versions[latest['family']] = latest['latest_version']
            logger.info('updated global: %s', global_family_versions)

            logger.info('Updating workspace')
            new_families = {}
            for family, version in workspace_family_versions.items():
                if family in global_family_versions:
                    new_families[family] = global_family_versions[family] + 1
                else:
                    new_families[family] = version + 1
            model.families = new_families
            GoogleJSONBackend._update_start_id(model)
            model.save()

        logger.info('Finished commit, released locks and transaction')

    def get_file(self, file_handler, **kwargs):
        """Obtain an open, readable file object.

        Note that this method is not part of a workspace object, because files
        added in CloudTS are saved in the global data resource. The
        metadata might be bound to the workspace.

        Parameters
        ----------
        file_handler: str
            Universal Resource Identifier of the file.

        Returns
        -------
        file-like
            A stream object to read the contents of the file.

        """
        workspace = kwargs.get('workspace', None)
        metadata = Metadata.objects\
                           .filter(file_handler=file_handler,
                                   family='cloudts',
                                   workspace=workspace)\
                           .last()
        # If the file is not present in this workspace, try on the global
        # workspace
        if metadata is None and workspace is not None:
            metadata = Metadata.objects\
                               .filter(file_handler=file_handler,
                                       family='cloudts',
                                       workspace=None,
                                       id__lte=workspace.start_id)\
                               .last()

        # If the file is still not found, it does not exist (it may exist on a
        # non-committed workspace, but that is private)
        if metadata is None:
            raise FileNotFoundException('File is {0} is not on workspace {1} or '
                                        'has not been commited to the global '
                                        'workspace'
                                        .format(file_handler, workspace.pk))

        url = metadata.kv_store['url']
        if url.startswith(self.data_uri):
            url = url.replace(self.data_uri, '').lstrip('/')
        else:
            raise FileNotFoundException('File is {0} is not on the data backend '
                                        '({1})'
                                        .format(file_handler, self.data_uri))

        # TODO: it would be better if we did not respond to get_file of large
        # files
        try:
            blob = self.bucket.blob(url)
            f = tempfile.SpooledTemporaryFile(mode='w+b', max_size=64*(1<<20))
            blob.download_to_file(f)
            f.flush()
            f.seek(0)
            return f
        except NotFound:
            raise FileNotFoundException('File {} not found'.format(file_handler))

    def get_metadata(self, file_handler, *, families, **kwargs):
        """Obtain the committed (global) metadata of a file

        Note that this method is not part of a workspace object. It is intended
        for the metadata that has already been committed.

        Parameters
        ----------
        file_handler: str
            Universal Resource Identifier of the file.

        families: dict
            A string to int dictionary representing the specific families and
            version for the request.

        Returns
        -------
        dict
            A dictionary where keys are families and values are dictionaries of
            key:value metadata.

        """
        queryset = Metadata.objects\
                           .filter(workspace__isnull=True,
                                   file_handler=file_handler)
        if not queryset.exists():
            # No file has empty metadata. If this query is empty, it means the
            # file does not exist on the global workspace
            raise FileNotFoundException('File {} does not exist on global workspace'
                                        .format(file_handler))
        return queryset

    def get_families(self, **kwargs):
        """Get all available metadata families and versions.

        Metadata families and versions are the metadata families that have been
        committed to the global workspace.

        Returns
        -------
        dict
            A dictionary where keys are family names and values are a list of
            available versions.

        """
        known = Metadata.objects\
                        .filter(workspace__isnull=True)\
                        .values('family')\
                        .annotate(versions=ArrayAgg('version'))
        families = {}
        for row in known:
            family_name = row['family']
            versions = set([0])
            versions.update(row['versions'])
            families[row['family']] = list(sorted(versions))
        return families


class GoogleJSONWorkspace(AbstractDataWorkspace, AbstractMetadataWorkspace):
    """ Workspace implementation that concretely saves data and metadata """

    def __init__(self, *, backend, workspace_model, prefix,
                 enforce_permissions=True, initialize=True):
        super().__init__(backend=backend)

        self.client = backend.client
        self.workspace_model = workspace_model
        # Check that the naming does not clash with our tables or is injectable
        schema_view = 'schema_{}'.format(workspace_model.name.replace('-', '_'))
        if not re.match('^[a-zA-Z0-9_]+$', schema_view):
            # Make sure that this name is not injectable
            raise ValueError('Invalid workspace name "{}"'.format(workspace_model.name))
        self.__schema_view = schema_view
        self.__schema_table = schema_view + '_ctable'
        self._check_names(set(self.workspace_model.families.keys()))
        # Bucket names cannot be larger than 63 characters
        basename = '{}-{}'.format(prefix, workspace_model.name)
        if len(basename) > 63:
            suffix = '-short'
            self.bucket_name = '{0}{1}'.format(basename[:(63-len(suffix))], suffix)
        else:
            self.bucket_name = basename
        if basename != self.bucket_name:
            logger.info('Shortened name from {} to {}'.format(basename, self.bucket_name))
        # Create bucket and initialize uris
        self.bucket = self.client.bucket(self.bucket_name)
        self.uri = 'gs://{}/'.format(self.bucket_name)
        # Initialize marker of the start_id
        if self.workspace_model.start_id is None:
            GoogleJSONBackend._update_start_id(self.workspace_model)
        # Initialize buckets et al.
        if initialize:
            self._initialize_resources()

    @property
    def schema_view(self):
        return self.__schema_view

    @property
    def schema_tables(self):
        return self.__schema_table

    @property
    def readonly_user(self):
        return self.backend.readonly_user

    def _check_names(self, names):
        if isinstance(names, str) or not isinstance(names, collections.Iterable):
            names = [names]
        for n in names:
            if not re.match('^[a-zA-Z_]+[a-zA-Z0-9_]*$', n) or \
               n == 'base_view':
                raise ValueError('Invalid name "{}"'.format(n))

    def _initialize_resources(self):
        bucket = None
        try:
            # # Create a schema on the database for the metadata views
            # with transaction.atomic(), connection.cursor() as cursor:
            #     sql = 'CREATE SCHEMA {0}'.format(self.schema_view)
            #     cursor.execute(sql)

            # Create the bucket on Google:
            # Instead of self.client.create_bucket(...), the following lines
            # permit to define the storage class and region
            #self.client.create_bucket(self.bucket_name)
            bucket = Bucket(self.client, name=self.bucket_name)
            bucket.storage_class = 'REGIONAL'
            bucket.location = self.backend.region
            bucket.create()

            # Set the bucket permissions
            owner_email = self.workspace_model.owner.email
            logger.info('Granting permissions to %s', owner_email)
            acl = bucket.acl
            acl.user(owner_email).grant_read()
            acl.user(owner_email).grant_write()
            acl.save()

        except Conflict as ex:
            logger.warning('Bucket {} already exists'.format(self.bucket_name),
                           exc_info=True)
            # Don't retry, because we wouldn't know how to retrieve the new
            # bucket name later. For the moment, just fail.
            raise ResourceAlreadyExistsException() from ex

        except:
            logger.warning('Failed to initialize workspace', exc_info=True)
            if bucket: bucket.delete()
            raise

    @property
    def data_uri(self):
        """ Unique Resource Identifier of the workspace. """
        return self.uri

    def delete(self):
        logger.info('Deleting bucket %s', self.bucket_name)
        if not self.bucket.exists():
            logger.info('Bucket %s does not exist', self.bucket_name)
        else:
            blobs = list(self.bucket.list_blobs())
            if blobs:
                logger.info('Deleting %d files existing in bucket', len(blobs))
                self.bucket.delete_blobs(blobs)
            self.bucket.delete(force=True)
        self.uri = None
        # Delete the schema on the database that had the metadata views
        with connection.cursor() as cursor:
            sql = 'DROP SCHEMA IF EXISTS {0} CASCADE'
            cursor.execute(sql.format(self.schema_view))
            cursor.execute(sql.format(self.schema_tables))

    def add_file(self, new_file, *, owner, permissions, filename=None, **kwargs):
        """ Add a new file to the data workspace.

        Adding a new file in a data backend entails the following
        operations. First, the contents of `new_file` will be copied to a new
        resource in the data workspace. Second, a set of basic metadata will be
        extracted from this file.

        Parameters
        ----------
        new_file: str or file-like
            The new file that will be added to the workspace.

        owner: str
            Username of the owner of the file.

        permissions: int
            Permissions for this file, as an octal number. This might change in
            the future.

        Returns
        -------
        str
            The URI that identifies the new file on the workspace.

        dict
            Automatic metadata extracted from the file.

        """
        # Check that the file is in this workspace when it's a path
        if not hasattr(new_file, 'read'):
            if new_file.startswith(self.data_uri):
                # File is already in this workspace data directory
                blob = self.bucket.get_blob(new_file.replace(self.data_uri, ''))
            elif new_file.startswith(self.backend.data_uri):
                # file is already in the global data directory
                blob = self.backend.bucket.get_blob(new_file.replace(self.backend.data_uri, ''))
            else:
                raise ValueError('Cannot add file outside datadir of workspace '
                                 'or global workspace')
            if blob is None:
                # blob is None if the use gave a URL from the data directories
                # but there is not really a file in that URL
                raise ValueError('Cannot add file {}: does not exist'
                                 .format(new_file))
            base_metadata = self.extract_metadata(blob)

        else:
            # File is not a path, but a file-like object
            blob = None
            # Before copying this file, we need to extract the automatic metadata
            base_metadata = self.extract_metadata(new_file)
            if filename is not None:
                base_metadata['filename'] = osp.split(filename)[-1]
                base_metadata['path'] = osp.split(filename)[0]
                base_metadata['extension'] = osp.splitext(filename)[-1][1:]

        # add additional metadata that depends on this request
        base_metadata['owner'] = owner

        # Generate a new handler for this file
        file_handler = self._generate_handle()

        # check if this file has already been added in the backend
        logger.debug('Checking if file exists with hash {hash} and size {size}'
                    .format(**base_metadata))

        previous = FileRecord.objects.filter(hash=base_metadata['hash'],
                                             size=base_metadata['size'])

        if previous.exists():
            # File exists in the data backend; do not copy it.
            exists = True
            logger.info('This file already exists, using the URI of {}'
                        .format(previous[0].url))
            file_url = previous[0].url

        else:
            exists = False
            logger.info('File does not exist, uploading it to bucket')
            # File does not exist in the data backend; copy it.
            if blob is None:
                new_blob = self.backend.bucket.blob(file_handler)
                new_file.seek(0)
                new_blob.upload_from_file(new_file)
            else:
                new_blob = self.bucket.copy_blob(blob, self.backend.bucket,
                                                 new_name=file_handler)
            file_url = 'gs://{bucket}/{handler}'.format(bucket=self.backend.bucket.name,
                                                        handler=file_handler)

        # Create the minimal metadata of this file
        base_metadata['id'] = file_handler
        base_metadata['url'] = file_url

        # Save the basic metadata in the CloudTS family
        logger.debug('Updating metadata...')
        metadata_instance = self.save_metadata(file_handler, 'cloudts',
                                               base_metadata, owner=owner,
                                               permissions=0o600)

        logger.debug('Finished adding file %s', file_url)
        return file_url, metadata_instance

    def extract_metadata(self, file_obj, **kwargs):
        """ Extract the automatic metadata of a file.

        Automatic metadata keys are: filename, path, suffix, size, hash, date
        and expiration date.

        Parameters
        ----------
        file_obj: file object

        Returns
        -------
        dict
            Automatic metadata extracted from the file.

        Notes
        -----
        Should this method be in a workspace or in the backend?

        """
        # FIXME: this should be a timezone aware date, such as
        # django.utils.timezone.localtime(django.utils.timezone.now())
        now = datetime.datetime.now()
        metadata = {
            'filename': None,
            'path': '',
            'extension' : '',
            'size': 0, #os.stat(name).st_size,
            'hash': None,
            'date': now.isoformat(),
            'expiration_date': (now + datetime.timedelta(days=7)).isoformat(), # TODO: parametrize this in settings
            #'owner': pwd.getpwuid(os.stat(name).st_uid).pw_name,
        }

        # file is a file-like descriptor: only extract the metadata that does
        # not require to read the stream
        if hasattr(file_obj, 'read'):
            metadata.update(get_readable_info(file_obj))
            return metadata

        elif not isinstance(file_obj, Blob):
            raise ValueError('Cannot extract metadata from {}'
                             .format(type(file_obj)))
        # assume that file_obj is a blob from here on...
        blob = file_obj

        path, filename = osp.split(blob.name)
        metadata['path'] = path

        # Fill the rest of the metadata available when the file is a regular
        # file
        metadata['filename'] = filename
        metadata['extension'] = osp.splitext(filename)[-1][1:]
        metadata['size'] = blob.size
        metadata['owner'] = blob.owner
        # Google stores the MD5 hash, but encoded in 64bits. The following
        # operation decodes it
        metadata['hash'] = binascii.hexlify(base64.b64decode(blob.md5_hash)).decode('utf-8')

        return metadata

    def create_family(self, name, *, owner, permissions, **kwargs):
        """Create a new family of metadata.

        Parameters
        ----------
        name: str
            Name of the family.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        uri: str
            Universal Resource Identifier of the family resource.

        """
        # Family creation: does it make sense for jsonb? probably not
        pass

    def save_metadata(self, file_id, family_name, mapping, *, owner, **kwargs):
        """Set the metadata key/values of a file under a family name.

        This method will replace whatever metadata existed before for `file_id`
        under the `family_name` family.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        family_name: str
            Name of the family.

        mapping: dict
            Key/value metadata.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        Metadata?

        """
        self._check_names(family_name)
        # First, check if the entry already exists:
        version = self.workspace_model.families.get(family_name, 0)
        try:
            metadata_instance = Metadata.objects\
                                        .get(file_handler=file_id,
                                             workspace=self.workspace_model.pk,
                                             family=family_name,
                                             version=version)
        except Metadata.DoesNotExist:
            # File does not exist: ensure that the mapping has an id on it
            if 'id' not in mapping:
                raise ValueError('Refusing to create new metadata without id')
            # When it doesn't exist, create it
            metadata_instance = Metadata(workspace=self.workspace_model,
                                         file_handler=file_id,
                                         family=family_name,
                                         version=version,
                                         kv_store={})

        # Overwrite existing metadata and save changes in database
        metadata_instance.kv_store = mapping
        metadata_instance.save()

        # Also update the workspace model if the metadata was new
        if family_name not in self.workspace_model.families:
            self.workspace_model.families[family_name] = version
            self.workspace_model.save()

        # Mark the workpace as dirty
        if not self.workspace_model.dirty:
            self.workspace_model.dirty = True
            self.workspace_model.save()

        return metadata_instance

    def update_metadata(self, file_id, family_name, mapping, *, owner, **kwargs):
        """Update the metadata key/values of a file under a family name.

        This method will update (not replace) whatever metadata existed before
        for `file_id` under the `family_name` family.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        family_name: str
            Name of the family.

        mapping: dict
            Key/value metadata.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        Metadata?

        """
        # Check that the file exists
        exists = Metadata.objects\
                         .filter(file_handler=file_id, family='cloudts')\
                         .filter(Q(workspace=self.workspace_model.pk) |
                                 (Q(workspace=None) & Q(id__lte=self.workspace_model.start_id)))\
                         .exists()
        if not exists:
            raise FileNotFoundException('File {} does not exist'.format(file_id))

        # Check that the names are allowed
        self._check_names(family_name)

        # Get the previous entry when it already exists:
        version = self.workspace_model.families.get(family_name, 0)
        try:
            metadata_instance = Metadata.objects\
                                        .get(file_handler=file_id,
                                             workspace=self.workspace_model.pk,
                                             family=family_name,
                                             version=version)
        except Metadata.DoesNotExist:
            # File metadata does not exist: ensure that the mapping has an id on it
            if 'id' not in mapping:
                raise ValueError('Refusing to create new metadata without id')
            # When it doesn't exist, create it
            metadata_instance = Metadata(workspace=self.workspace_model,
                                         file_handler=file_id,
                                         family=family_name,
                                         version=version,
                                         kv_store={})

        # Update existing metadata and save changes in database
        logger.debug('Updating metadata of %s family %s\nfrom %s\nto %s',
                     file_id, family_name, metadata_instance.kv_store, mapping)
        metadata_instance.kv_store.update(mapping)
        logger.debug('New metadata is %s', metadata_instance.kv_store)
        metadata_instance.save()

        # Also update the workspace model if the metadata was new
        if family_name not in self.workspace_model.families:
            self.workspace_model.families[family_name] = version
            # Mark the workpace as dirty
            if not self.workspace_model.dirty:
                self.workspace_model.dirty = True
            self.workspace_model.save()

        return metadata_instance.pk

    def metadata(self, *, families=None, **kwargs):
        """Get all the metadata entries in the backend.

        This function returns a dictionary with at least three levels of depth.
        On the first level (the most shallow), keys are file handlers.  On the
        second level, keys are family names. The last level (the most deep), is
        a dictionary of key/value metadata.

        Parameters
        ----------
        families: set of strings
            When not ``None``, it determines which families will be included in
            the result. Otherwise, all families are returned.

        Returns
        -------
        dict
            See long description for details of the structure of this
            dictionary.

        """
        if families is None:
            family_set = set(self.workspace_model.families.keys())
        else:
            family_set = set(families)
        return Metadata.objects\
                       .filter(workspace=self.workspace_model.pk,
                               family__in=family_set)

    def get_metadata(self, file_id, **kwargs):
        """Get all the metadata entries of a file.

        This method will retrieve all families and key/value entries for a file.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        Returns
        -------
        dict
            A dictionary where keys are family names and values are dictionaries
            with key/value pairs of metadata.

        """
        queryset = Metadata.objects\
                           .filter(workspace=self.workspace_model.pk,
                                   file_handler=file_id)
        if not queryset.exists():
            # No file has empty metadata. If this query is empty, it means the
            # file does not exist on this workspace
            raise FileNotFoundException('File {} does not exist on workspace {}'
                                        .format(file_id, self.workspace_model.pk))
        return queryset

    def families(self, **kwargs):
        """Get all families and their respective versions in this workspace.

        If a workspace introduces a new family, it will have version 0.

        Returns
        -------
        dict
            A dictionary where keys are family names, values are the number of
            the version used in this workspace.

        """
        return Metadata.objects\
                       .filter(workspace=self.workspace_model.pk)

    def query(self, sql, fast=None, **kwargs):
        """Query the metadata of this workspace.

        Queries can be regular SQL, but a specific backend might limit to a
        subset or superset of SQL if necessary.

        On the SQL, metadata families should be considered as tables, metadata
        keys are columns and metadata values are the contents of each
        cell. Family versions are the version declared for this workspace, they
        need not be specified. An example SQL could be::

            SELECT
                cloudts.url as url,
                cloudts.size as filesize,
                cloudts.filename as filename,
                family.condition as condition
            FROM
                cloudts, family
            WHERE
                cloudts.id == family.id
            ORDER BY
                filesize

        This function could depend on resources initialized on :py:func:`scan`.

        Parameters
        ----------
        sql: str
            The SQL query.

        fast: bool
            When `True`, use the fast (but potentially out of date) schema to
            execute the query. Otherwise, use the slower (but up to date) schema.
            When `None` use the fast approach but only if the workspace is not dirty

        Returns
        -------
        to be determined

        """
        if fast is None:
            # This value is under observation. By default using True is great
            # for faster execution, but it could pose some problems if the
            # metadata is not up to date. We still haven't had this problem
            # but if it turns out to be an important problem, we will sacrifice
            # speed in favor of consistency here, by setting the previous
            # approach:
            # fast = not self.workspace_model.dirty
            fast = True
            logger.info('Fast query option not set, using %s', fast)

        #with connections['readonly'].cursor() as cursor: # This is not working yet, a problem with permissions
        with connection.cursor() as cursor:
            # Save the current search path, in order to restablish it later
            cursor.execute('SHOW search_path')
            current_path = cursor.fetchone()[0]
            logger.info('Current search path was %s', current_path)
            # Set the search path to only the namespace where the views live
            # TODO: protect here from injection, just in case
            if fast:
                schema = self.schema_tables
            else:
                schema = self.schema_view
            cursor.execute('SET search_path = {}'.format(schema))
            try:
                logger.info('Executing {}'.format(sql))
                # Perform the query
                # https://docs.djangoproject.com/en/1.11/topics/db/sql/#executing-custom-sql-directly
                cursor.execute(sql)
                # TODO / TOTHINK perhaps make a yield generator?
                col_names = [desc[0] for desc in cursor.description]
                results = []
                while True:
                    row = cursor.fetchone()
                    if row is None:
                        break
                    row_dict = dict(zip(col_names, row))
                    results.append(row_dict)
            except:
                logger.warning('Something went wrong on a query', exc_info=True)
                raise
            finally:
                # Reset the search path to the correct configuration
                cursor.execute('SET search_path = {}'.format(current_path))
        return results

    def scan(self, **kwargs):
        """Initialize resources for querying the metadata of the workspace.

        Metadata might be represented in different ways on a
        backend. Particularly, since metadata is not well structured, it is
        likely that it does not exist in a relational database. This function
        prepares some kind of resource to be able to query the metadata.

        """
        logger.info('Scanning workspace {}'.format(self.workspace_model))

        # Prepare the query that includes the interesting metadata for this
        # workspace
        logger.info('Analysing associated metadata...')

        # Build the conditions to obtain the latest metadata on the global
        # workspace
        conditions = []
        params = [self.workspace_model.pk, self.workspace_model.start_id]
        for name, version in self.workspace_model.families.items():
            conditions.append('family = %s')
            params.append(name)

        with transaction.atomic(), connection.cursor() as cursor:

            logger.info('Dropping existing schema {0}'.format(self.schema_view))
            # Note: schema names are not passable by parameter, so we ensure
            # that schema_view is not injectable at the constructor and the
            # property accessor
            drop_schema_sql = 'DROP SCHEMA IF EXISTS {0} CASCADE'
            cursor.execute(drop_schema_sql.format(self.schema_view))
            cursor.execute(drop_schema_sql.format(self.schema_tables))

            # Create a new schema
            logger.info('Creating schema {0}'.format(self.schema_view))
            create_schema_sql = 'CREATE SCHEMA {0}'
            cursor.execute(create_schema_sql.format(self.schema_view))
            cursor.execute(create_schema_sql.format(self.schema_tables))

            # If it's a temp table, the name of the table is unimportant:
            # https://stackoverflow.com/a/33657970/227103
            # However, I don't know if views support using a temporary table: R: NO!
            # Maybe create a view on a view ? OK!
            # CREATE TABLE mytable AS
            # Other notes: two selects in because we want to
            # "order by" then "group by"
            sql = \
                """
                CREATE VIEW {schema}.base_view AS
                SELECT file_handler, MAX(workspace_id) AS workspace_id,
                       family, MAX(version) AS version,
                       jsonb_merge(kv_store ORDER BY version) AS kv_store
                FROM cloudts_metadata
                WHERE
                    (workspace_id = %s)
                    OR
                    (workspace_id IS NULL AND id <= '%s' AND ({global_family_conditions}))
                GROUP BY (file_handler, family)
                """.format(
                    schema=self.schema_view,
                    global_family_conditions=' OR '.join(conditions),
                )

            # Create view
            cursor.execute(sql, params)
            cursor.execute('SELECT COUNT(*) FROM {schema}.base_view'
                           .format(schema=self.schema_view))
            count = cursor.fetchone()[0]
            logger.info('There are {} entries'.format(count))
            if count == 0:
                logger.info('Empty metadata, no views created')
                return

            # # Clear the previous schema
            # logger.info('Dropping existing schema {0}'.format(self.schema_view))
            # # Note: schema names are not passable by parameter, so we ensure
            # # that schema_view is not injectable at the constructor and the
            # # property accessor
            # drop_schema_sql = 'DROP SCHEMA IF EXISTS {0} CASCADE'.format(self.schema_view)
            # cursor.execute(drop_schema_sql)

            # # Create a new schema
            # logger.info('Creating schema {0}'.format(self.schema_view))
            # create_schema_sql = 'CREATE SCHEMA {0}'.format(self.schema_view)
            # cursor.execute(create_schema_sql)

            # Create a view for each family
            for family_name, family_version in self.workspace_model.families.items():
                logger.info('Creating view for family {} at version {}'
                            .format(family_name, family_version))

                # Check that this family is not empty
                cursor.execute('SELECT COUNT(*) FROM {schema}.base_view WHERE family = %s'
                               .format(schema=self.schema_view),
                               (family_name, ))
                count = cursor.fetchone()[0]
                if count == 0:
                    logger.info('No view for family {}: no entries found'
                                .format(family_name))
                    continue

                structure_sql = \
                    """
                    SELECT format(
                         'SELECT h->>%s
                          FROM  (SELECT family, kv_store AS h FROM {schema}.base_view) t
                          WHERE family = ''{family}'';'
                        , string_agg(quote_literal(key) || ' AS ' || quote_ident(key), ', h->>')
                       ) AS sql
                    FROM  (
                       SELECT DISTINCT key
                       FROM {schema}.base_view, jsonb_object_keys(kv_store) key
                       WHERE family = '{family}'
                       ORDER  BY 1
                       ) sub;
                    """.format(schema=self.schema_view, family=family_name)
                    # """
                    # SELECT format(
                    #      'SELECT h->%s
                    #       FROM  (SELECT id, kv_store AS h FROM cloudts_metadata) t;'
                    #     , string_agg(quote_literal(key) || ' AS ' || quote_ident(key), ', h->')
                    #    ) AS sql
                    # FROM  (
                    #    SELECT DISTINCT key
                    #    FROM   cloudts_metadata, jsonb_object_keys(kv_store) key
                    #    ORDER  BY 1
                    #    ) sub;
                    # """
                cursor.execute(structure_sql)
                row = cursor.fetchone()
                if row is None:
                    logger.info('Not enough metadata to create view')
                else:
                    logger.info('Got structure as {}'.format(row[0].replace('\n', ' ')))
                    create_view_sql = \
                        """
                        CREATE VIEW {schema}.{family_name} AS
                        {structure_query}
                        """.format(schema=self.schema_view,
                                   family_name=family_name,
                                   structure_query=row[0])
                    logger.info('Create view is {}'.format(create_view_sql.replace('\n', ' ')))
                    cursor.execute(create_view_sql)

                # Copy of the view: a faster access to the metadata but not up-to-date
                logger.info('Creating copy of family view for %s', family_name)
                create_table_sql = 'SELECT * INTO {schema_tables}.{table_name} FROM {schema_views}.{table_name}'
                cursor.execute(create_table_sql.format(schema_tables=self.schema_tables,
                                                       schema_views=self.schema_view,
                                                       table_name=family_name))
                create_index_sql = 'CREATE UNIQUE INDEX {table_name}_idx ON {schema_tables}.{table_name} (id)'
                cursor.execute(create_index_sql.format(schema_tables=self.schema_tables,
                                                       table_name=family_name))

                # Permissions: add a read only access for the query but not to the base view
                cursor.execute('GRANT SELECT ON ALL TABLES IN SCHEMA {schema} TO {user}'
                               .format(schema=self.schema_view,
                                       user=self.readonly_user))
                cursor.execute('GRANT SELECT ON ALL TABLES IN SCHEMA {schema} TO {user}'
                               .format(schema=self.schema_tables,
                                       user=self.readonly_user))
                cursor.execute('REVOKE SELECT ON TABLE {schema}.base_view FROM {user}'
                               .format(schema=self.schema_view,
                                       user=self.readonly_user))

            # Mark the workpace as clean
            self.workspace_model.dirty = False
            self.workspace_model.save()
