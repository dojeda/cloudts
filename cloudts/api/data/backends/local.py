# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""

THIS BACKEND IS OUTDATED AND DEPRECATED. DO NOT USE. IT WILL BE REMOVED.

USE THE google_pg BACKEND!

"""

import collections
import datetime
import glob
import io
import json
import logging
import os
import os.path as osp
import pickle
import pwd
import sh
import shutil
import subprocess
import threading
import urllib

import pandas as pd
from pandasql import sqldf

from cloudts.api.data.models import FileRecord

from .base import AbstractBackend, AbstractDataWorkspace, AbstractMetadataWorkspace
from .exceptions import ResourceAlreadyExistsException, CommitConflictException
from .utils import deep_update, flatten, update_values, get_readable_info


logger = logging.getLogger(__name__)


def _get_user_ids(username):
    """ Get the UID and GID of an existing user """
    try:
        uid = pwd.getpwnam(username).pw_uid
        gid = pwd.getpwnam(username).pw_gid
    except KeyError:
        logger.warning('Unknown user %s', username, exc_info=True)
        raise
    return uid, gid

def _copy_file(src, dest):
    """ Copy the contents of a file to another

    Parameters
    ----------
    src: str or file-like
        The source file

    dest: str
        The destination file

    Returns
    -------
    None

    Notes
    -----
    If `src` is a file-like buffer, it will be consumed after this call.

    """
    if hasattr(src, 'read'):
        f_src = src
        with open(dest, 'wb') as f_dest:
            f_dest.write(f_src.read())
    else:
        if src.startswith('file:'):
            src = urllib.parse.urlparse(src).path
        with open(src, 'rb') as f_src, open(dest, 'wb') as f_dest:
            f_dest.write(f_src.read())


class LocalBackend(AbstractBackend):
    """ Backend implementation using a local filesystem"""

    def __init__(self, *, basedir, enforce_permissions=True):
        # DataBackend initialization
        super().__init__()
        self._enforce_permissions = enforce_permissions
        # Threading lock for the operations that cannot be concurrent
        self._mutex = threading.Lock()
        # Create base directory
        if not osp.isabs(basedir):
            raise ValueError('basedir of LocalBackend must be an absolute path')
        self.basedir = self._prepare_dir(basedir, permissions=0o755)
        # Create directory for commited data files
        self.datadir = self._prepare_dir(osp.join(self.basedir, 'data'),
                                         permissions=0o755)
        # Create directory for committed metadata files
        self.metadatadir = self._prepare_dir(osp.join(self.basedir, 'metadata'),
                                             permissions=0o755)
        # Create directory for workspaces
        self.workdir = self._prepare_dir(osp.join(self.basedir, 'workspaces'),
                                         permissions=0o755)

    @property
    def permissions_enabled(self):
        return self._enforce_permissions

    @property
    def data_uri(self):
        return self.datadir

    @property
    def metadata_uri(self):
        return self.metadatadir

    def new_user(self, username):
        if not self.permissions_enabled:
            logger.debug('Permissions are not enforced: no system user created')
            return
        try:
            uid = pwd.getpwnam(username).pw_uid
            exists = True
        except KeyError:
            exists = False

        if not exists:
            logger.debug('System user {0} does not exist, creating it...'
                  .format(username))
            output = sh.bash(osp.join(osp.split(__file__)[0],
                                      'local_init_user.sh'),
                             username)
            print(output)

    def create_workspace(self, *, model):
        workspace = LocalWorkspace(backend=self,
                                   name=model.name,
                                   owner=model.owner.get_username(),
                                   families=model.families,
                                   basedir=self.workdir,
                                   enforce_permissions=self.permissions_enabled,
                                   initialize=True)
        return workspace

    def retrieve_workspace(self, *, model):
        workspace = LocalWorkspace(backend=self,
                                   name=model.name,
                                   owner=model.owner.get_username(),
                                   families=model.families,
                                   basedir=self.workdir,
                                   enforce_permissions=self.permissions_enabled,
                                   initialize=False)
        return workspace

    def delete_workspace(self, *, model):
        workspace = self.retrieve_workspace(model=model)
        workspace.delete()

    def commit(self, *, model, **kwargs):
        # sketch:
        # 0. lock the backend so that there are no other modifications during
        # this time
        # 1. get the latest families and versions
        # 2. get the families and versions of the workspace
        # 3. get the list of files and metadata
        # 4. manage conflicts, abort if there are any
        # 5. copy files to global/public workspace
        # 6. udpate metadata references
        # 7. drop useless metadata changes
        # 8. create new metadata files in ther version+1 directory

        workspace = self.retrieve_workspace(model=model)
        logger.debug('Attempting to commit workspace {}, data={}, metadata={}'
                     .format(workspace.uri, workspace.datadir, workspace.metadatadir))


        # 0. lock the backend so that there are no other modifications during
        # this time
        with self._mutex:
            # 1. get the latest families and versions
            global_family_versions = self._get_families()
            logger.debug('Global family versions are {}'
                         .format(global_family_versions))

            # 2. get the families and versions of the workspace
            workspace_family_versions = workspace.families()
            logger.debug('Workspace family versions are {}'
                         .format(workspace_family_versions))

            # 3. get the list of files and metadata
            metadata = workspace.metadata()
            files = [osp.split(f)[-1] for f in workspace.files()]

            # 4. manage conflicts, abort if there are any
            conflicts = {}
            for family, version_list in global_family_versions.items():
                if family not in workspace_family_versions:
                    continue
                latest_version = max(version_list)
                workspace_version = workspace_family_versions[family]
                if latest_version != workspace_version:
                    logger.debug('Conflict for {}: {} != {}'
                                 .format(family,
                                         workspace_version,
                                         latest_version))
                    conflicts[family] = ('Workspace version for {} '
                                         'is out of date: '
                                         'workspace version = {} '
                                         'global version = {}'
                                         .format(family,
                                                 workspace_version,
                                                 latest_version))
            if conflicts:
                raise CommitConflictException(conflicts)

            # 5. copy files to global/public workspace
            # Not necessary, files are already in the global/public workspace,
            # but the permissions must be fixed
            new_urls = {}
            for file_id in metadata:
                if file_id not in files:
                    continue
                src = osp.join(workspace.datadir, file_id)
                dest = osp.join(self.datadir, file_id)
                new_urls[src] = dest
                logger.info('{} -> {}'.format(src, dest))
                #shutil.copy(src, dest)

            # 6. udpate metadata references
            # Not necessary, files are already in the global/public workspace.
            # metadata = update_values(metadata, new_urls)

            # 7. drop useless metadata changes
            # TODO

            # 8. create new metadata files in ther version+1 directory
            for file_id, file_metadata in metadata.items():
                if file_id not in files:
                    continue
                for family, family_metadata in file_metadata.items():
                    version = global_family_versions.get(family, [0])[-1] + 1
                    version_dir = osp.join(self.metadatadir, family,
                                           str(version))
                    os.makedirs(version_dir, exist_ok=True)
                    json_file = osp.join(version_dir, file_id + '.json')
                    logger.debug('Creating {}'.format(json_file))
                    with open(json_file, 'w') as f:
                        json.dump(family_metadata, f, sort_keys=True)

    def get_families(self):
        with self._mutex:
            return self._get_families()

    def _get_families(self):
        dirs = next(os.walk(self.metadatadir))[1]
        families = {}
        for d in dirs:
            subdirs = os.listdir(osp.join(self.metadatadir, d))
            versions = [int(_s) for _s in subdirs]
            families[d] = list(sorted(versions))
        if 'cloudts' not in families:
            families['cloudts'] = [0]
        return families

    def get_file(self, file_handler, **kwargs):
        file_uri = osp.join(self.datadir, file_handler)
        return io.open(file_uri, 'rb')

    def get_metadata(self, file_handler, *, families, **kwargs):
        # TODO: this should look onto the global metadata space for global
        # metadata of a file as well!
        metadata = {}

        for family, version in families.items():
            filename = osp.join(self.metadatadir, family, str(version),
                                file_handler + '.json')
            if not osp.exists(filename):
                raise FileNotFoundError('Metadata for file {0}, family {1}, version {2} '
                                        'does not exist'.format(file_handler, family, version))

            with open(filename, 'r') as f:
                metadata[family] = json.load(f)

        return metadata

    def _prepare_dir(self, dirname, permissions=None):
        """ Create directory and set its permissions """
        dirname = os.path.abspath(dirname)
        os.makedirs(dirname, exist_ok=True)
        if not osp.exists(dirname):
            raise ValueError('{0} does not exist'.format(dirname))
        if not osp.isdir(dirname):
            raise ValueError('{0} is not a directory'.format(dirname))
        if self.permissions_enabled and permissions is not None:
            os.chmod(dirname, permissions)
        return dirname


class LocalWorkspace(AbstractDataWorkspace, AbstractMetadataWorkspace):

    def __init__(self, *, backend, name, owner, basedir, families,
                 initialize=True,  enforce_permissions=True):
        super().__init__(backend=backend)
        self.uri = osp.join(basedir, owner, name)
        self.datadir = osp.join(self.uri, 'data')
        self.metadatadir = osp.join(self.uri, 'metadata')
        self.tables = osp.join(self.metadatadir, 'tables.pickle')
        self._families = families.copy()
        self._enforce_permissions = enforce_permissions

        # Initialize the underlying directories
        if initialize:
            self._create_dirs(owner)
            self._initialize_metadata(families, owner)
            self.scan()

    def delete(self):
        shutil.rmtree(self.uri)
        self.uri = self.datadir = self.metadatadir = None

    @property
    def permissions_enabled(self):
        return self._enforce_permissions

    @property
    def data_uri(self):
        return self.datadir

    def add_file(self, new_file, *, owner, permissions, filename=None, **kwargs):
        # Check that the file is in this workspace when it's a path
        if not hasattr(new_file, 'read'):
            # File is not a stream, it must be a URL
            parsed_url = urllib.parse.urlparse(new_file)
            if not parsed_url.scheme == 'file':
                raise ValueError('Only file:// urls are allowed')
            if not os.path.isabs(parsed_url.path):
                raise ValueError('Only absolute paths are allowed')
            logger.info('path is {} data dir {} backend data {}'\
                        .format(parsed_url.path,
                                self.data_uri,
                                self.backend.datadir))
            if not parsed_url.path.startswith(self.data_uri) and \
               not parsed_url.path.startswith(self.backend.data_uri):
                raise ValueError('Cannot add file outside backend data directory or '
                                 'workspace data directory')

            # TODO: extract metadata from this file
            base_metadata = self.extract_metadata(new_file)
            logger.info('base metadata %s', base_metadata)

        else:
            # Before copying this file, we need to extract the automatic metadata
            base_metadata = self.extract_metadata(new_file)

        # Generate a new handler for this file
        file_handler = self._generate_handle()

        # add additional metadata that depends on this request
        base_metadata['owner'] = owner
        if hasattr(new_file, 'read') and filename is not None:
            base_metadata['filename'] = os.path.split(filename)[-1]
            base_metadata['path'] = os.path.split(filename)[0]
            base_metadata['suffix'] = os.path.splitext(filename)[-1][1:]

        # check if this file has already been added in the backend
        logger.debug('Checking if file exists with hash {hash} and size {size}'
                    .format(**base_metadata))
        existing_records = FileRecord.objects.filter(hash__exact=base_metadata['hash'],
                                                     size__exact=base_metadata['size'])
        if existing_records.exists():
            # File exists in the data backend; do not copy it.
            previous = existing_records[0]
            logger.info('This file already exists, using the URI of {}'
                        .format(previous.uri))
            file_url = existing_records[0].uri

        else:
            # File does not exist in the data backend; copy it.
            target_uri = osp.join(self.backend.data_uri, file_handler)
            _copy_file(new_file, target_uri)
            file_url = 'file://{0}'.format(target_uri)

            if self.permissions_enabled:
                try:
                    uid, fid = _get_user_ids(owner)
                    os.chown(uid, gid)
                    os.chmod(target_uri, permissions)
                except:
                    logger.warning('Could not copy file', exc_info=True)
                    raise

        # Update id and url metadata
        base_metadata['id'] = file_handler
        base_metadata['url'] = file_url

        # Save the basic metadata in the CloudTS family
        self.save_metadata(file_handler, 'cloudts', base_metadata, owner=owner,
                           permissions=0o600)

        # Update the file record, but only if it didn't exist before
        if not existing_records.exists():
            record = FileRecord(file_handler=file_handler,
                                uri=base_metadata['url'],
                                hash=base_metadata['hash'],
                                size=base_metadata['size'])
            record.save()

        logger.debug('Added file %s', file_url)
        return file_url, base_metadata

    def extract_metadata(self, file, **kwargs):
        # FIXME: this should be a timezone aware date, such as
        # django.utils.timezone.localtime(django.utils.timezone.now())
        now = datetime.datetime.now()
        metadata = {
            'filename': None,
            'path': '',
            'suffix' : '',
            'size': 0, #os.stat(name).st_size,
            'hash': None,
            'date': now.isoformat(),
            'expiration_date': (now + datetime.timedelta(days=7)).isoformat(), # TODO: parametrize this in settings
            #'owner': pwd.getpwuid(os.stat(name).st_uid).pw_name,
        }

        # file is a file-like descriptor: only extract the metadata that does
        # not require to read the stream
        if hasattr(file, 'read'):
            metadata.update(get_readable_info(file))
            return metadata

        if not isinstance(file, str):
            raise ValueError('Cannot extract metadata from {}'
                             .format(type(file)))

        file = urllib.parse.urlparse(file).path

        if not os.path.isabs(file):
            raise ValueError('Unexpected relative path')

        if not file.startswith(self.datadir) and not file.startswith(self.backend.data_uri):
            raise ValueError('Cannot extract metadata of file outside '
                             'data backend or workspace')
        if file.startswith(self.datadir):
            metadata['path'] = osp.split(osp.relpath(file, start=self.datadir))[0]
        else:
            metadata['path'] = ''

        # Fill the rest of the metadata available when the file is a regular
        # file
        metadata['filename'] = osp.split(file)[1]
        metadata['suffix'] = osp.splitext(file)[-1][1:]
        metadata['size'] = os.stat(file).st_size
        metadata['owner'] = pwd.getpwuid(os.stat(file).st_uid).pw_name

        # Get the metadata that requires reading the file
        with open(file, 'rb') as f:
            metadata.update(get_readable_info(f))

        return metadata

    def create_family(self, name, *, owner, permissions, **kwargs):
        family_url = osp.join(self.metadatadir, name)
        try:
            os.makedirs(family_url, mode=permissions, exist_ok=True)
            if self.permissions_enabled:
                uid, gid = _get_user_ids(owner)
                os.chown(family_url, uid, gid)
                os.chmod(family_url, permissions)
        except:
            logger.warning('Could not create metadata family',
                           exc_info=True)
            raise
        return family_url

    def get_metadata(self, file_id, **kwargs):
        dirs = next(os.walk(self.metadatadir))[1]
        metadata = {}
        exists = False
        for d in dirs:
            family_metadata = {}
            json_file = osp.join(self.metadatadir, d, file_id + '.json')
            if os.path.exists(json_file):
                exists = True
                with open(json_file, 'r') as f:
                    family_metadata = json.load(f)
            metadata[d] = family_metadata
        if not exists:
            raise FileNotFoundError
        return metadata

    def save_metadata(self, file_id, family_name, mapping, *, owner, **kwargs):
        family_url = self.create_family(family_name, owner=owner,
                                        permissions=0o700)
        metadata_file = osp.join(family_url, file_id + '.json')
        # Handle existing metadata
        if osp.exists(metadata_file):
            with open(metadata_file, 'r') as fd:
                previous_metadata = json.load(fd)
        else:
            previous_metadata = {}
        previous_metadata = deep_update(previous_metadata, mapping)
        previous_metadata['id'] = file_id
        # Save to file in pretty format
        with open(metadata_file, 'w') as fd:
            json.dump(previous_metadata, fd, indent=4, sort_keys=True)
        return metadata_file

    def update_metadata(self, file_id, family_name, mapping, **kwargs):
        return self.save_metadata(file_id, family_name, mapping, **kwargs)

    def metadata(self, *, families=None):
        # Get all the metadata that is already committed, but only on the
        # families of this workspace
        if families is not None:
            global_families = {f:v for f, v in families.items() if f in families}
        else:
            global_families = self.families()
        global_metadata = self._collect_global_metadata(families=global_families)

        # Get all the metadata of this workspace
        if families is not None:
            local_families = set(families)
        else:
            local_families = set(self.families())
        local_metadata = self._collect_local_metadata(families=local_families)

        # Update local metadata over global
        all_metadata = global_metadata
        for local_id, local_entry in local_metadata.items():
            if local_id not in all_metadata:
                all_metadata[local_id] = local_entry
            else:
                existing_entry = all_metadata[local_id]
                for family in local_entry:
                    if family not in existing_entry:
                        existing_entry[family] = local_entry[family]
                    else:
                        # NOTE: if we want to implement some sort of tombstone
                        # value to mark metadata deletion, this is one of the
                        # places to do the check
                        existing_entry[family].update(local_entry[family])

        return all_metadata

    def _collect_global_metadata(self, *, families):
        all_metadata = dict()
        for family, version in families.items():
            # Get all files and versions in one single list to iterate later.
            # This is slightly better than a triple-loop
            files = sum([glob.glob('{0}/{1}/{2}/*.json'.format(self.backend.metadatadir,
                                                              family,
                                                              v))
                        for v in  range(version+1)], [])
            # The files in the list should be sorted by family and version, so
            # now we can read each file and update whatever was read before.
            for f in files:
                file_handler = osp.splitext(osp.basename(f))[0]
                if file_handler not in all_metadata:
                    all_metadata[file_handler] = {}
                file_metadata = all_metadata[file_handler]
                if family not in file_metadata:
                    file_metadata[family] = {}
                family_metadata = file_metadata[family]
                with open(f, 'r') as fd:
                    md = json.load(fd)
                # NOTE: if we want to implement some sort of tombstone value to
                # mark metadata deletion, this is one of the places to do the
                # check
                family_metadata.update(md)

        return all_metadata

    def _collect_local_metadata(self, *, families):
        all_metadata = dict()
        for json_file in glob.glob('{0}/*/*.json'.format(self.metadatadir)):
            tmp, filename = osp.split(json_file)
            _, family = osp.split(tmp)
            if families is not None and family not in families:
                continue
            file_id, _ = osp.splitext(filename)
            file_metadata = all_metadata.get(file_id, {})
            with open(json_file, 'r') as f:
                file_metadata[family] = json.load(f)
                all_metadata[file_id] = file_metadata

        return all_metadata

    def files(self):
        # TODO: do we want/need this ???

        # Note: The way to determine the files in a workspace is to see what
        # JSON files have been added to the metadata.
        json_files = glob.glob('{0}/*/*.json'.format(self.metadatadir))
        file_handlers = set(osp.splitext(f)[0] for f in json_files)
        return file_handlers

    def families(self):
        d = self._families.copy()
        new_families = next(os.walk(self.metadatadir))[1]
        for family in new_families:
            if family in d:
                continue
            d[family] = 0
        return d


    def query(self, sql, **kwargs):
        with open(self.tables, 'rb') as f:
            databases = pickle.load(f)

        df = sqldf(sql, databases)

        return [collections.OrderedDict(row) for _, row in df.iterrows()]

    def scan(self, **kwargs):
        metadata = self.metadata()
        databases = {}
        for file_id, families in metadata.items():
            for family, metadata in families.items():
                if family not in databases:
                    databases[family] = []
                db = databases[family]
                db.append(flatten(metadata))

        for family in databases:
            df = pd.DataFrame.from_records(databases[family])
            df.sort_values(by='id', inplace=True)
            df.reset_index(drop=True, inplace=True)
            databases[family] = df

        logger.debug('Creating database tables at %s', self.tables)
        with open(self.tables, 'wb') as f:
            pickle.dump(databases, f)

    def _create_dirs(self, owner):
        """ Create the directories needed for a workspace"""
        logger.debug('Creating directories for %s at %s', owner, self.uri)
        try:
            os.makedirs(self.uri, exist_ok=False)
            os.makedirs(self.datadir, exist_ok=False)
            os.makedirs(self.metadatadir, exist_ok=False)
            if self.permissions_enabled:
                owner_uid, owner_gid = _get_user_ids(owner)
                for dirname in (self.uri, self.datadir, self.metadatadir):
                    os.chown(dirname, owner_uid, owner_gid)
                    os.chmod(dirname, 0o700)
        except FileExistsError as ex:
            logger.warning('Directory already exists', exc_info=True)
            raise ResourceAlreadyExistsException('Workspace directory already exists')

        except:
            logger.warning('Could not create data workspace directory',
                           exc_info=True)
            raise

    def _initialize_metadata(self, families, owner):
        logger.debug('initializing metadata: {}'.format(families))
        for family in families:
            self.create_family(family, owner=owner, permissions=0o700)
        # for family, version in families.items():
        #     family_dir = osp.join(self.metadatadir, family)
        #     os.makedirs(family_dir, exist_ok=False)
        #     if self.permissions_enabled:
        #         owner_uid, owner_gid = _get_user_ids(owner)
        #         os.chown(family_dir, owner_uid, owner_gid)
        #         os.chmod(family_dir, 0o700)
            # # get the most recent metadata
            # all_metadata = {}
            # for i in range(version+1):
            #     for filename in glob.glob(osp.join(source_dir, family, str(i), '*.json')):
            #         file_id = osp.splitext(osp.split(filename)[-1])[0]
            #         with open(filename, 'r') as f:
            #             d = json.load(f)
            #         if file_id not in all_metadata:
            #             all_metadata[file_id] = {}
            #         all_metadata[file_id].update(d)
            # # copy the most recent metadata
            # for file_id, metadata in all_metadata.items():
            #     filename = osp.join(self.metadatadir, family, file_id + '.json')
            #     logger.debug('initializing metadata -> {}'.format(filename))
            #     with open(filename, 'w') as f:
            #         json.dump(metadata, f, sort_keys=True)

    def _initialize_tables(self):
        pass
        #with open(
