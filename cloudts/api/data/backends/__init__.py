# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from importlib import import_module
import warnings

from django.conf import settings


def get_backend():
    """ Get the instance of the backend configured for CloudTS Data API """
    engine = settings.CLOUDTS_CONFIG['data_backend']['ENGINE']
    options = settings.CLOUDTS_CONFIG['data_backend'].get('OPTIONS', {})
    module_path, class_name = engine.rsplit('.', 1)
    module = import_module(module_path)
    klass = getattr(module, class_name)
    return klass(**options)
