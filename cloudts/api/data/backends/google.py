# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""

THIS BACKEND IS OUTDATED AND DEPRECATED. DO NOT USE. IT WILL BE REMOVED.

USE THE google_pg BACKEND!

"""


import base64
import binascii
import datetime
import glob
import io
import json
import logging
import os
import os.path as osp
import pickle
import pwd
import shutil
import subprocess
import tempfile
import textwrap
import threading
import warnings


from google.cloud.storage import Client, Bucket, Blob
import google.cloud.exceptions

from cloudts.api.data.models import FileRecord

from .base import AbstractBackend, AbstractDataWorkspace, AbstractMetadataWorkspace
from .exceptions import ResourceAlreadyExistsException, CommitConflictException
from .local import LocalBackend, LocalWorkspace
from .utils import deep_update, get_readable_info


logger = logging.getLogger(__name__)



#TODO: extend local to avoid repeating code
class GoogleBackend(LocalBackend):
    """ Backend that uses Google storage for data and local files for metadata"""

    def __init__(self, *, bucket_name, local_basedir, json_credentials, region,
                 prefix, enforce_permissions=True):
        # DataBackend initialization
        if enforce_permissions:
            warnings.warn('Permissions not currently implemented in GoogleBackend',
                          UserWarning)
        super().__init__(basedir=local_basedir, enforce_permissions=False)

        # Initialize Google API client
        logging.info('attempting to use json credentials %s', json_credentials)
        self.client = Client.from_service_account_json(json_credentials)
        self.bucket = self.client.get_bucket(bucket_name)
        self.region = region
        self.prefix = prefix

        # Remote base directory
        self.remote_basedir = 'gs://{}/'.format(bucket_name)

    @property
    def data_uri(self):
        return self.remote_basedir

    def new_user(self, username):
        logger.info('new_user on the GoogleBackend is currently no-op')

    def create_workspace(self, *, model):
        workspace = GoogleLocalWorkspace(backend=self,
                                         name=model.name,
                                         owner=model.owner.get_username(),
                                         families=model.families,
                                         basedir=self.workdir,
                                         enforce_permissions=self.permissions_enabled,
                                         prefix=self.prefix,
                                         initialize=True)
        return workspace

    def retrieve_workspace(self, *, model):
        workspace = GoogleLocalWorkspace(backend=self,
                                         name=model.name,
                                         owner=model.owner.get_username(),
                                         families=model.families,
                                         basedir=self.workdir,
                                         enforce_permissions=self.permissions_enabled,
                                         prefix=self.prefix,
                                         initialize=False)
        return workspace

    def delete_workspace(self, *, model):
        workspace = self.retrieve_workspace(model=model)
        workspace.delete()

    def get_file(self, file_handler, **kwargs):
        # TODO: it would be better if we did not respond to get_file of large
        # files
        blob = self.bucket.blob(file_handler)
        f = tempfile.SpooledTemporaryFile(mode='w+b', max_size=64*(1<<20))
        blob.download_to_file(f)
        f.flush()
        f.seek(0)
        return f


class GoogleLocalWorkspace(LocalWorkspace):

    def __init__(self, *, backend, name, owner, basedir, families,
                 prefix, initialize=True,  enforce_permissions=True):

        self.client = backend.client
        # Bucket names cannot be larger than 63 characters
        basename = '{0}-{1}-{2}'.format(prefix, name, owner)
        if len(basename) > 63:
            suffix = '-short'
            self.bucket_name = '{0}{1}'.format(basename[:(63-len(suffix))], suffix)
        else:
            self.bucket_name = basename
        if basename != self.bucket_name:
            logger.info('Shortened name from {} to {}'.format(basename, self.bucket_name))
        self.bucket = self.client.bucket(self.bucket_name)

        # call constructor but don't initialize anything yet
        super().__init__(backend=backend, name=name, owner=owner, basedir=basedir,
                         families=families, initialize=False,
                         enforce_permissions=enforce_permissions)
        self.uri = 'gs://{}/'.format(self.bucket_name)
        self.datadir = self.uri

        # Initialize the underlying directories
        if initialize:
            self._create_dirs(owner)
            self._initialize_metadata(families, owner)
            self.scan()

    def delete(self):
        self.bucket.delete(force=True)
        shutil.rmtree(self.metadatadir)
        self.uri = self.datadir = self.metadatadir = None


    def add_file(self, new_file, *, owner, permissions, filename=None, **kwargs):
        # Check that the file is in this workspace when it's a path
        if not hasattr(new_file, 'read'):
            if new_file.startswith(self.data_uri):
                blob = self.bucket.get_blob(new_file.replace(self.data_uri, ''))
            elif new_file.startswith(self.backend.data_uri):
                blob = self.backend.bucket.get_blob(new_file.replace(self.backend.data_uri, ''))
            else:
                raise ValueError('Cannot add file outside datadir of workspace '
                                 'or global workspace')
            if blob is None:
                raise ValueError('Cannot add file {}: does not exist'
                                 .format(new_file))
            base_metadata = self.extract_metadata(blob)
        else:
            blob = None
            # Before copying this file, we need to extract the automatic metadata
            base_metadata = self.extract_metadata(new_file)
            if filename is not None:
                base_metadata['filename'] = os.path.split(filename)[-1]
                base_metadata['path'] = os.path.split(filename)[0]
                base_metadata['suffix'] = os.path.splitext(filename)[-1][1:]

        # add additional metadata that depends on this request
        base_metadata['owner'] = owner

        # Generate a new handler for this file
        file_handler = self._generate_handle()

        # check if this file has already been added in the backend
        logger.debug('Checking if file exists with hash {hash} and size {size}'
                    .format(**base_metadata))
        existing_records = FileRecord.objects.filter(hash__exact=base_metadata['hash'],
                                                     size__exact=base_metadata['size'])
        if existing_records.exists():
            # File exists in the data backend; do not copy it.
            previous = existing_records[0]
            logger.info('This file already exists, using the URI of {}'
                        .format(previous.uri))
            file_url = existing_records[0].uri

        else:
            logger.info('File does not exist, uploading it to bucket')
            # File does not exist in the data backend; copy it.
            if blob is None:
                new_blob = self.backend.bucket.blob(file_handler)
                new_file.seek(0)
                new_blob.upload_from_file(new_file)
            else:
                new_blob = self.bucket.copy_blob(blob, self.backend.bucket,
                                                 new_name=file_handler)

        # Update id metadata
        file_url = 'gs://{bucket}/{handler}'.format(bucket=self.backend.bucket.name,
                                                    handler=file_handler)
        base_metadata['id'] = file_handler
        base_metadata['url'] = file_url

        # Save the basic metadata in the CloudTS family
        logger.debug('Updating metadata...')
        self.save_metadata(file_handler, 'cloudts', base_metadata, owner=owner,
                           permissions=0o600)

        # Update the file record, but only if it didn't exist before
        if not existing_records.exists():
            logger.debug('Adding record')
            record = FileRecord(file_handler=file_handler,
                                uri=base_metadata['url'],
                                hash=base_metadata['hash'],
                                size=base_metadata['size'])
            record.save()

        logger.debug('Finished adding file %s', file_url)
        return file_url, base_metadata

    def extract_metadata(self, file, **kwargs):
        # FIXME: this should be a timezone aware date, such as
        # django.utils.timezone.localtime(django.utils.timezone.now())
        now = datetime.datetime.now()
        metadata = {
            'filename': None,
            'path': '',
            'suffix' : '',
            'size': 0, #os.stat(name).st_size,
            'hash': None,
            'date': now.isoformat(),
            'expiration_date': (now + datetime.timedelta(days=7)).isoformat(), # TODO: parametrize this in settings
            #'owner': pwd.getpwuid(os.stat(name).st_uid).pw_name,
        }

        # file is a file-like descriptor: only extract the metadata that does
        # not require to read the stream
        if hasattr(file, 'read'):
            metadata.update(get_readable_info(file))
            return metadata

        if not isinstance(file, Blob):
            raise ValueError('Cannot extract metadata from {}'
                             .format(type(file)))

        path, filename = osp.split(file.name)
        metadata['path'] = path

        # Fill the rest of the metadata available when the file is a regular
        # file
        metadata['filename'] = filename
        metadata['suffix'] = osp.splitext(filename)[-1][1:]
        metadata['size'] = file.size
        metadata['owner'] = file.owner
        # Google stores the MD5 hash, but encoded in 64bits. The following
        # operation decodes it
        metadata['hash'] = binascii.hexlify(base64.b64decode(file.md5_hash)).decode('utf-8')

        return metadata

    def _create_dirs(self, owner):
        """ Create the directories needed for a workspace"""
        logger.debug('Creating directories for %s at %s', owner, self.uri)
        try:
            # Instead of self.client.create_bucket(...), the following lines
            #permit to define the storage class and region
            #self.client.create_bucket(self.bucket_name)
            bucket = Bucket(self.client, name=self.bucket_name)
            bucket.storage_class = 'REGIONAL'
            bucket.location = self.backend.region
            bucket.create()

            # Create the metadata directory, which is saved locally
            os.makedirs(self.metadatadir, exist_ok=False)
            # if self.permissions_enabled:
            #     owner_uid, owner_gid = _get_user_ids(owner)
            #     for dirname in (self.uri, self.datadir, self.metadatadir):
            #         os.chown(dirname, owner_uid, owner_gid)
            #         os.chmod(dirname, 0o700)
        except google.cloud.exceptions.Conflict as ex:
            logger.warning('Bucket already exists', exc_info=True)
            raise ResourceAlreadyExistsException('Bucket for workspace already exists')
        except FileExistsError as ex:
            logger.warning('Directory already exists', exc_info=True)
            raise ResourceAlreadyExistsException('Workspace directory already exists')

        except:
            logger.warning('Could not create data workspace directory',
                           exc_info=True)
            raise
