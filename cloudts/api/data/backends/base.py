# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""
Abstract class definitions for a backend in CloudTS.
"""


import abc
import uuid

from cloudts.api.singleton import Singleton


class AbstractBackend(metaclass=Singleton):
    """ Abstract class that defines a data and metadata backend on CloudTS. """

    @abc.abstractproperty
    def permissions_enabled(self):
        """ Whether the backend instance enforces permissions. """
        pass

    @abc.abstractproperty
    def data_uri(self):
        """ Unique Resource Identifier for the location of public data """
        pass


    @abc.abstractproperty
    def metadata_uri(self):
        """ Unique Resource Identifier for the location of public metadata """
        pass

    @abc.abstractmethod
    def new_user(self, username, **kwargs):
        """Register a new user on the backend

        Some backends need to create or initialize a user in order to enforce
        the permissions. This function performs the initialization of whatever
        resource needed to create a user.

        Parameters
        ----------
        username: str
            The username of the new user.

        Returns
        -------
        None

        """
        pass

    @abc.abstractmethod
    def create_workspace(self, *, model):
        """Create a workspace and initialize its resources.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the creation of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        object
            An object instance that implements AbstractDataWorkspace and
            AbstractMetadataWorkspace.

        """
        pass

    @abc.abstractmethod
    def retrieve_workspace(self, *, model):
        """Retrieve an existing workspace.

        This method should work like create_workspace, but without the
        initialization of the resources.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the retrieval of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        object
            An object instance that implements AbstractDataWorkspace and
            AbstractMetadataWorkspace.

        """
        pass

    @abc.abstractmethod
    def delete_workspace(self, *, model):
        """ Delete all resources of an existing workspace.

        Parameters
        ----------
        model: object
            An object with the attributes needed for the retrieval of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        None

        """
        pass

    @abc.abstractmethod
    def commit(self, *, model, **kwargs):
        """ Commit the data and metadata of a workspace.

        model: object
            An object with the attributes needed for the creation of the
            workspace. The specific attributes of the object are
            implementation-dependent.

        Returns
        -------
        None

        """
        pass

    @abc.abstractmethod
    def get_file(self, file_handler, **kwargs):
        """Obtain an open, readable file object.

        Note that this method is not part of a workspace object, because files
        added in CloudTS are saved in the global data resource. The
        metadata might be bound to the workspace.

        Parameters
        ----------
        file_handler: str
            Universal Resource Identifier of the file.

        Returns
        -------
        file-like
            A stream object to read the contents of the file.

        """
        pass

    @abc.abstractmethod
    def get_metadata(self, file_handler, *, families, **kwargs):
        """Obtain the committed (global) metadata of a file

        Note that this method is not part of a workspace object. It is intended
        for the metadata that has already been committed.

        Parameters
        ----------
        file_handler: str
            Universal Resource Identifier of the file.

        families: dict
            A string to int dictionary representing the specific families and
            version for the request.

        Returns
        -------
        dict
            A dictionary where keys are families and values are dictionaries of
            key:value metadata.

        """
        pass

    @abc.abstractmethod
    def get_families(self, **kwargs):
        """Get all available metadata families and versions.

        Metadata families and versions are the metadata families that have been
        committed to the global workspace.

        Returns
        -------
        dict
            A dictionary where keys are family names and values are a list of
            available versions.

        """
        pass


class AbstractDataWorkspace(metaclass=abc.ABCMeta):
    """ Abstract class definition of a data workspace. """

    def __init__(self, *, backend, **kwargs):
        """ Create a data workspace.

        Parameters
        ----------
        backend: AbstractBackend
            Reference to the backend object that creates an instance of a data
            workspace. This is useful for cases when the data workspace needs
            information that is represented in the backend object.

        """
        self._backend_reference = backend

    @property
    def backend(self):
        """ Reference to the backend object that created this workspace. """
        return self._backend_reference

    @abc.abstractproperty
    def data_uri(self):
        """ Unique Resource Identifier of the workspace. """
        pass

    @abc.abstractmethod
    def add_file(self, new_file, *, owner, permissions, **kwargs):
        """ Add a new file to the data workspace.

        Adding a new file in a data backend entails the following
        operations. First, the contents of `new_file` will be copied to a new
        resource in the data workspace. Second, a set of basic metadata will be
        extracted from this file.

        Parameters
        ----------
        new_file: str or file-like
            The new file that will be added to the workspace.

        owner: str
            Username of the owner of the file.

        permissions: int
            Permissions for this file, as an octal number. This might change in
            the future.

        Returns
        -------
        str
            The URI that identifies the new file on the workspace.

        dict
            Automatic metadata extracted from the file.

        """
        pass

    @abc.abstractmethod
    def extract_metadata(self, uri, **kwargs):
        """ Extract the automatic metadata of a file.

        Automatic metadata keys are: filename, path, suffix, size, hash, date
        and expiration date.

        Parameters
        ----------
        uri: str
            Universal Resource Identifier of the file.

        Returns
        -------
        dict
            Automatic metadata extracted from the file.

        Notes
        -----
        Should this method be in a workspace or in the backend?

        """
        pass

    def _generate_handle(self):
        """ Generate a random file handle """
        random_id = str(uuid.uuid4())
        return '{0}'.format(random_id)


class AbstractMetadataWorkspace(metaclass=abc.ABCMeta):
    """Abstract class definition of a metadata workspace."""

    def __init__(self, *, backend, **kwargs):
        """Create a metadata workspace.

        Parameters
        ----------
        backend: AbstractBackend
            Reference to the backend object that creates an instance of a
            metadata workspace. This is useful for cases when the metadata
            workspace needs information that is represented in the backend
            object.

        """
        self._backend_reference = backend

    @property
    def backend(self):
        """Reference to the backend object that created this workspace."""
        return self._backend_reference

    @abc.abstractmethod
    def create_family(self, name, *, owner, permissions, **kwargs):
        """Create a new family of metadata.

        Parameters
        ----------
        name: str
            Name of the family.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        uri: str
            Universal Resource Identifier of the family resource.

        """
        pass

    @abc.abstractmethod
    def save_metadata(self, file_id, family_name, mapping, *, owner, **kwargs):
        """Set the metadata key/values of a file under a family name.

        This method will replace whatever metadata existed before for `file_id`
        under the `family_name` family.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        family_name: str
            Name of the family.

        mapping: dict
            Key/value metadata.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        str
            An URI to the resource associated with the new metadata entry.

        """
        pass

    @abc.abstractmethod
    def update_metadata(self, file_id, family_name, mapping, *, owner, **kwargs):
        """Update the metadata key/values of a file under a family name.

        This method will update (not replace) whatever metadata existed before
        for `file_id` under the `family_name` family.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        family_name: str
            Name of the family.

        mapping: dict
            Key/value metadata.

        owner: str
            Owner of the resource associated with the family. Do we need this?
            I think this should be fixed to the workspace owner and not a
            parameter.

        permissions: int
            Permissions of the resource associated with the family. Do we need
            this? I think that this should be fixed 0o700 and not a parameter.

        Returns
        -------
        str
            An URI to the resource associated with the updated metadata entry.

        """
        pass

    @abc.abstractmethod
    def metadata(self, *, families=None, **kwargs):
        """Get all the metadata entries in the backend.

        This function returns a dictionary with at least three levels of depth.
        On the first level (the most shallow), keys are file handlers.  On the
        second level, keys are family names. The last level (the most deep), is
        a dictionary of key/value metadata.

        Parameters
        ----------
        families: set of strings
            When not ``None``, it determines which families will be included in
            the result. Otherwise, all families are returned.

        Returns
        -------
        dict
            See long description for details of the structure of this
            dictionary.

        """
        pass

    @abc.abstractmethod
    def get_metadata(self, file_id, **kwargs):
        """Get all the metadata entries of a file.

        This method will retrieve all families and key/value entries for a file.

        Parameters
        ----------
        file_id: str
            File handler of the file.

        Returns
        -------
        dict
            A dictionary where keys are family names and values are dictionaries
            with key/value pairs of metadata.

        """
        pass

    @abc.abstractmethod
    def families(self, **kwargs):
        """Get all families and their respective versions in this workspace.

        If a workspace introduces a new family, it will have version 0.

        Returns
        -------
        dict
            A dictionary where keys are family names, values are the number of
            the version used in this workspace.

        """
        pass

    @abc.abstractmethod
    def query(self, sql, **kwargs):
        """Query the metadata of this workspace.

        Queries can be regular SQL, but a specific backend might limit to a
        subset or superset of SQL if necessary.

        On the SQL, metadata families should be considered as tables, metadata
        keys are columns and metadata values are the contents of each
        cell. Family versions are the version declared for this workspace, they
        need not be specified. An example SQL could be::

            SELECT
                cloudts.url as url,
                cloudts.size as filesize,
                cloudts.filename as filename,
                family.condition as condition
            FROM
                cloudts, family
            WHERE
                cloudts.id == family.id
            ORDER BY
                filesize

        This function could depend on resources initialized on :py:func:`scan`.

        Parameters
        ----------
        sql: str
            The SQL query.

        Returns
        -------
        to be determined

        """
        pass

    @abc.abstractmethod
    def scan(self, **kwargs):
        """Initialize resources for querying the metadata of the workspace.

        Metadata might be represented in different ways on a
        backend. Particularly, since metadata is not well structured, it is
        likely that it does not exist in a relational database. This function
        prepares some kind of resource to be able to query the metadata.

        """
        pass
