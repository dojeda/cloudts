# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" Common functions useful for any data backend """

import collections
import hashlib


def deep_update(d, u):
    """ Update a dictionary to add the (key, values) of another dictionary """
    # Inspired from http://stackoverflow.com/a/3233356/227103
    for k, v in u.items():
        if isinstance(v, collections.Mapping) and \
           isinstance(d.get(k, {}), collections.Mapping):
            d[k] = deep_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


def flatten(mapping):
    """ Flatten a dictionary with nested entries """
    flat = {}
    for k, v in mapping.items():
        if isinstance(v, collections.Mapping):
            flat_v = flatten(v)
            flat.update({'{base}.{rest}'.format(base=k, rest=_k): _v
                         for _k, _v in flat_v.items()})
        else:
            flat[k] = str(v)  # Workaround for weird values
    return flat


def update_values(mapping, new_values):
    """ Change the values of dictionary according to another dictionary """
    for k, v in mapping.items():
        if isinstance(v, collections.Mapping):
            mapping[k] = update_values(v, new_values)
        elif v in new_values:
            mapping[k] = new_values[v]
    return mapping


def get_readable_info(file_obj):
    """ Extract useful information from reading a file

    This includes the hash (md5) and the size of the file.

    """
    info = dict()
    size = 0
    position = file_obj.tell()
    hashobj = hashlib.new('md5')
    while True:
        chunk = file_obj.read(4096)
        size += len(chunk)
        if not chunk:
            break
        hashobj.update(chunk)
    file_obj.seek(position)
    info['hash'] = hashobj.hexdigest()
    info['size'] = size
    return info
