# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
# ###################################

"""cloudts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from rest_framework_nested.routers import DefaultRouter, NestedDefaultRouter

from cloudts.api.data import views


urlpatterns = []

# A router creates URLs and associates them with the correct method of a
# viewset.
router = DefaultRouter()
router.include_format_suffixes = False

## Router configuration for workspaces and their files
# /workspaces/
router.register(r'workspaces',
                views.WorkspaceViewSet,
                base_name='workspace')

# /workspaces/<pk>/files/
files_router = NestedDefaultRouter(router,
                                   parent_prefix=r'workspaces',
                                   lookup='workspace')
files_router.include_format_suffixes = False
files_router.include_root_view = False
files_router.register(r'files',
                      views.FileViewSet,
                      base_name='workspace-files')

## Router configuration for files not in a workspace (i.e. already commited in
## the global workspace)
# /files/
router.register(r'files',
                views.FileViewSet,
                base_name='file')

# Note that the resources
# /workspaces/<pk>/scan/
# /workspaces/<pk>/query/
# /workspaces/<pk>/commit/
# are all managed by a detail_route on the WorkspaceViewSet
#
# Also note that the resources:
# /workspaces/<workspace_pk>/files/exists/
# /files/exists/
# are managed by a list_route on the FileViewSet

urlpatterns += router.urls
urlpatterns += files_router.urls
