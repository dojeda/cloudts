# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

from django.apps import AppConfig

from .backends import get_backend


class DataAPIConfig(AppConfig):
    name = 'cloudts.api.data'
    verbose_name = 'CloudTS data API'
    label = 'cloudts_data'

    def ready(self):
        # initialize backends if necessary. Backends are singleton so this will
        # be slow only this first time when initializing the application.
        # IMPORTANT/weirdness: if we do not initialize it here, for some reason,
        # we will get a segmentation fault whenever the backend is really used.
        data_backend = get_backend()

