# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""Django REST Framework viewsets for CloudTS Data API files and metadata"""

import logging
from wsgiref.util import FileWrapper

from django.http import Http404
from django_celery_results.models import TaskResult
from rest_framework import viewsets, renderers, status, views, permissions
from rest_framework.decorators import list_route
from rest_framework.generics import get_object_or_404, UpdateAPIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from cloudts.api.data.backends.exceptions import FileNotFoundException
from cloudts.api.data.exceptions import DataAPIException
from cloudts.api.data.models import Workspace, Metadata, FileRecord, get_backend
from cloudts.api.data.permissions import IsOwnerOrReadOnly
from cloudts.api.data.serializers import FileCreateSerializer, MetadataSerializer, \
    MetadataUpdateSerializer, MetadataBatchUpdateSerializer, \
    FileRecordQuerySerializer
from cloudts.api.job.tasks import metadata_batch_update
from cloudts.api.job.serializers import JobSerializer


logger = logging.getLogger(__name__)


class BinaryRenderer(renderers.BaseRenderer): # pylint: disable=too-few-public-methods
    """Renderer of binary content for file contents"""
    media_type = 'application/octet-stream'
    charset = None
    format = None
    render_style = 'binary'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """ Render the data. No particular modification is done here"""
        return data


class FileViewSet(viewsets.GenericViewSet, UpdateAPIView):
    """ DRF for upload of files and management of their metadata """

    permission_classes = (permissions.IsAuthenticated,
                          IsOwnerOrReadOnly, )

    # First things first: disallow PUT and DELETE
    def _disabled(self, *args, **kwargs):  # pylint: disable=unused-argument,no-self-use
        raise DataAPIException(
            detail='Method not allowed',
            code='method_not_allowed',
            status_code=status.HTTP_405_METHOD_NOT_ALLOWED)

    update = _disabled
    destroy = _disabled

    def get_queryset(self):
        workspace_pk = self.kwargs.get('workspace_pk', None)
        if workspace_pk is None:
            return Metadata.objects.filter(workspace=None)
        return Metadata.objects.filter(workspace__pk=workspace_pk)

    def get_parent_object(self):
        """Get the workspace object associated with a file

        This method also checks the permissions of the authenticated use with
        respect to the workspace owner

        """
        workspace_pk = self.kwargs.get('workspace_pk', None)
        if workspace_pk is None:
            return None
        parent_queryset = Workspace.objects.all()
        obj = get_object_or_404(parent_queryset, pk=workspace_pk)
        self.check_object_permissions(self.request, obj)
        return obj

    def create(self, request, workspace_pk=None):
        """Upload a file from the contents of a request or a URL"""
        # Creating a file without a workspace is not permitted
        if workspace_pk is None:
            return self._disabled()

        # Get parent and verify its permissions. We cannot use get_object()
        # because this viewset is associated to the Metadata object, not to the
        # Workspace object
        try:
            workspace = self.get_parent_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        serializer = FileCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        input_parameters = serializer.validated_data
        print(input_parameters)
        if request.FILES and 'url' in input_parameters:
            raise DataAPIException('Use the "url" parameter or an attachment, '
                                   'but not both.',
                                   code='invalid_request',
                                   status_code=status.HTTP_400_BAD_REQUEST)

        elif request.FILES:
            if 'file' not in request.FILES:
                raise DataAPIException('file variable should be named "file"',
                                       code='required',
                                       status_code=status.HTTP_400_BAD_REQUEST)
            file_object = request.FILES['file']
            filename = file_object.name

        elif 'url' in input_parameters:
            file_object = input_parameters['url']
            filename = None

        else:
            raise DataAPIException('Cannot create file without '
                                   '"file" or "url" contents',
                                   code='error',
                                   status_code=status.HTTP_400_BAD_REQUEST)

        logger.info('Adding file to %s', workspace)

        # Creation of file in the backend
        backend = get_backend()
        ws_object = backend.retrieve_workspace(model=workspace)
        _, metadata = ws_object.add_file(file_object,
                                         owner=self.request.user.get_username(),
                                         permissions=0o600,
                                         filename=filename)

        # Prepare a success response
        logger.debug('Finding reverse URL')
        reverse_url = reverse('workspace-files-detail',
                              kwargs=dict(workspace_pk=workspace.name,
                                          pk=metadata.file_handler),
                              request=request)
        headers = {
            'Location': reverse_url
        }
        serializer = MetadataSerializer(metadata)
        return Response(data=serializer.data, headers=headers,
                        status=status.HTTP_201_CREATED)

    def list(self, request, workspace_pk=None): # pylint: disable=unused-argument,no-self-use
        """List existing files in CloudTS?

        This has not been specified nor implemented.
        """
        raise DataAPIException('Listing files is not implemented',
                               code='not_implemented',
                               status_code=status.HTTP_501_NOT_IMPLEMENTED)

    def get_renderers(self):
        """
        Instantiates and returns the list of renderers that this view can use.
        """
        renderer_classes = super().get_renderers()
        if self.action == 'retrieve':
            renderer_classes.append(BinaryRenderer())
        return renderer_classes

    def retrieve(self, request, workspace_pk=None, pk=None): # pylint: disable=unused-argument
        """Get the contents or metadata of a file"""
        # Get parent and verify its permissions. We cannot use get_object()
        # because this viewset is associated to the Metadata object, not to the
        # Workspace object
        try:
            workspace = self.get_parent_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        # This view has two cases:
        # a. Retrieve the file contents. It is assumed that this is the case
        # when the accepted media type is application/octet-stream
        # b. Retrieve the file metadata. In any other accepted media type case
        backend = get_backend()

        if request.accepted_media_type == 'application/octet-stream':
            # The request demands binary content, we will serve the file
            if request.data:
                raise DataAPIException('Requests for file contents do not accept '
                                       'any input data',
                                       code='error',
                                       status_code=status.HTTP_400_BAD_REQUEST)

            # Get the file directly from the backend
            try:
                file_obj = backend.get_file(pk, workspace=workspace)
                wrapper = FileWrapper(file_obj)
                return Response(data=wrapper,
                                content_type='application/octet-stream')
            except FileNotFoundException as ex:
                raise DataAPIException(detail=str(ex),
                                       code='does_not_exist',
                                       status_code=status.HTTP_404_NOT_FOUND)
            except:
                raise DataAPIException(detail='Unexpected error',
                                       code='error',
                                       status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            # Assume the request wants the file's metadata

            # First, let's try on this workspace if the workspace was set
            if workspace is not None:
                try:
                    ws_object = backend.retrieve_workspace(model=workspace)
                    metadata = ws_object.get_metadata(pk)

                except FileNotFoundException as ex:
                    # Set the workspace to None so that we query on the global
                    # workspace in the next lines
                    workspace = None

            # Not an else, because workspace can be set to none just before
            if workspace is None:
                # The workspace is actually optional; when it's not specified, we
                # respond with the global metadata
                families = backend.get_families()
                try:
                    metadata = backend.get_metadata(pk, families=families)
                except FileNotFoundException as ex:
                    raise DataAPIException(detail='File {} does not exist'
                                           .format(pk),
                                           code='does_not_exist',
                                           status_code=status.HTTP_404_NOT_FOUND)

            serializer = MetadataSerializer(metadata)
            return Response(data=serializer.data, status=status.HTTP_200_OK)


    def partial_update(self, request, workspace_pk=None, pk=None):
        """Update the metadata of a file"""
        # Modifying a file's metadata without a workspace is not permitted
        if workspace_pk is None:
            return self._disabled()

        # Get parent and verify its permissions. We cannot use get_object()
        # because this viewset is associated to the Metadata object, not to the
        # Workspace object
        try:
            workspace = self.get_parent_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        serializer = MetadataUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_metadata = serializer.validated_data['metadata']

        # At this point, the input has been validated, we can update the metadata
        backend = get_backend()
        ws_object = backend.retrieve_workspace(model=workspace)
        try:
            for family in new_metadata:
                new_metadata[family]['id'] = pk
                ws_object.update_metadata(pk, family, new_metadata[family],
                                          owner=self.request.user.get_username(),
                                          permissions=0o600)
            metadata_result = ws_object.get_metadata(pk)
            known_families = set(new_metadata)
            known_families.add('cloudts')
            # Only respond with the metadata families that were modified in
            # this update and the cloudts family
            metadata_result = metadata_result.filter(family__in=known_families)

            # Update the workspace object if there were new families included
            changed = False
            for family in new_metadata:
                if family not in workspace.families:
                    workspace.families[family] = 0
                    changed = True
            if changed:
                workspace.save()

        except FileNotFoundException as ex:
            # This is thrown when the file_handler does not correspond to an existing file
            raise DataAPIException(detail=str(ex),
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)
        except Exception as ex:
            raise DataAPIException(detail='Unexpected error: '+str(ex),
                                   code='error',
                                   status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

        output_serializer = MetadataSerializer(metadata_result)
        return Response(output_serializer.data)

    # Disable put so that the batch update of metadata can only be done through
    # a PATCH method, since UpdateAPIView defines both put and patch
    put = _disabled

    def patch(self, request, workspace_pk=None):
        # Modifying a file's metadata without a workspace is not permitted
        if workspace_pk is None:
            return self._disabled()

        # Get parent and verify its permissions. We cannot use get_object()
        # because this viewset is associated to the Metadata object, not to the
        # Workspace object
        try:
            workspace = self.get_parent_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        # Read and validate the input request
        input_serializer = MetadataBatchUpdateSerializer(data=request.data, many=True)
        input_serializer.is_valid(raise_exception=True)

        # Create an asynchronous task to batch update, because this could take
        # some time
        task = metadata_batch_update.delay(workspace.pk, input_serializer.validated_data)
        task_result = TaskResult.objects.get_task(task.id)
        task_result.save()
        serializer = JobSerializer(task_result)

        return Response(data=serializer.data,
                        status=status.HTTP_200_OK)

    @list_route(methods=['get'], url_path='exists')
    def exists(self, request, workspace_pk=None): # pylint: disable=unused-argument
        """Checks if a file with a hash and size is known to CloudTS"""
        # Get parent and verify its permissions. We cannot use get_object()
        # because this viewset is associated to the Metadata object, not to the
        # Workspace object
        try:
            workspace = self.get_parent_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        # serializer = FileRecordQuerySerializer(data=request.data)
        # serializer.is_valid(raise_exception=True)
        # input_parameters = serializer.validated_data

        view = FileRecordView.as_view()
        if workspace is not None:
            return view(request, workspace_pk=workspace.pk)
        return view(request)


class FileRecordView(views.APIView):
    """An APIView to check existence of a file

    This implements the /check/ action on workspace files or global files.
    """

    def get(self, request, workspace_pk=None):
        """Implementation of the check of a known file in CloudTS

        The check uses the FileRecord view and will consider the workspace as
        well. This implementation is common to a workspace view and file view

        """
        input_serializer = FileRecordQuerySerializer(data=request.data)
        input_serializer.is_valid(raise_exception=True)
        input_parameters = input_serializer.data

        file_records = FileRecord.objects.filter(hash=input_parameters['hash'],
                                                 size=input_parameters['size'],
                                                 workspace_id=workspace_pk)
        if not file_records.exists():
            # 404 is a valid response for this resource, so don't raise an
            # exception but respond appropriately
            #raise DataAPIException(detail='Not found',
            #                       status_code=status.HTTP_404_NOT_FOUND)
            return Response(status=status.HTTP_404_NOT_FOUND)

        # Only respond with the first match
        output_serializer = FileRecordQuerySerializer(file_records.first())
        return Response(output_serializer.data,
                        status=status.HTTP_200_OK)
