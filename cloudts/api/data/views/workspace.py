# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""Django REST Framework viewsets for CloudTS Data API workspaces"""

import logging

from django.http import Http404
from django_celery_results.models import TaskResult
from rest_framework import mixins, viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.decorators import detail_route

from cloudts.api.data.exceptions import DataAPIException
from cloudts.api.data.models import Workspace, get_backend
from cloudts.api.data.permissions import IsOwnerOrReadOnly
from cloudts.api.data.serializers import WorkspaceSerializer, \
    QuerySerializer, QueryResultSerializer
from cloudts.api.job.tasks import workspace_scan, workspace_commit
from cloudts.api.job.serializers import JobSerializer


logger = logging.getLogger(__name__)


class WorkspaceViewSet(mixins.CreateModelMixin, # pylint: disable=too-many-ancestors
                       mixins.RetrieveModelMixin,
                       mixins.ListModelMixin,
                       mixins.DestroyModelMixin,
                       viewsets.GenericViewSet):
    """DRF Viewset for a workspace"""

    serializer_class = WorkspaceSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly,
    )

    def get_queryset(self):
        # Using the request query parameters and this queryset permits to do a
        # URL/?owner=xxx API resource. This is useful for listing workspaces.
        # For everything else, the permission class will deny access
        queryset = Workspace.objects.all()
        username = self.request.query_params.get('owner', None)
        if username is not None:
            queryset = queryset.filter(owner__username=username)
        return queryset

    def perform_create(self, serializer):
        # The code that creates the workspace resources in the backends could be
        # written here, in the serializer.save or in models.Workspace. However,
        # it is preferable to do it on models.Workspace since this is may not be
        # the only endpoint where workspaces are created. However, we can catch
        # an exception, log it and and wrap it under a DataAPIException
        try:
            serializer.save(owner=self.request.user)
        except DataAPIException:
            # Propagate the DataAPIException
            raise
        except:
            logger.error('Could not create workspace', exc_info=True)
            raise DataAPIException(detail='Unexpected error',
                                   code='error',
                                   status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    # TODO: find out why the router does not find this method in the browsable root API
    @detail_route(methods=['post'], url_path='commit')
    def commit(self, request, pk=None): # pylint: disable=invalid-name,unused-argument
        """ Commit a workspace """
        # Important! Use the self.get_object() so that DRF handles the permissions
        # of this object
        try:
            workspace = self.get_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        logger.info('Commit request on %s', workspace)

        # Create an asynchronous task to commit the workspace
        # TODO: robustness if rabbitmq is down... Right now it gets blocked
        task = workspace_commit.delay(workspace.name)
        task_result = TaskResult.objects.get_task(task.id)
        serializer = JobSerializer(task_result)

        return Response(data=serializer.data,
                        status=status.HTTP_200_OK)

    @detail_route(methods=['post'], url_path='scan')
    def scan(self, request, pk=None): # pylint: disable=invalid-name,unused-argument
        """Prepare the views for querying a workspace"""
        # Important! Use the self.get_object() so that DRF handles the permissions
        # of this object
        try:
            workspace = self.get_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        logger.info('Scan request on %s', workspace)

        # Create an asynchronous task to scan the workspace
        # TODO: robustness if rabbitmq is down... Right now it gets blocked
        task = workspace_scan.delay(workspace.pk)
        task_result = TaskResult.objects.get_task(task.id)
        task_result.save()
        serializer = JobSerializer(task_result)

        return Response(data=serializer.data,
                        status=status.HTTP_200_OK)

    @detail_route(methods=['post'], url_path='update')
    def update_families(self, request, pk=None):
        # Important! Use the self.get_object() so that DRF handles the permissions
        # of this object
        try:
            workspace = self.get_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)

        logger.info('Update request on %s', workspace)
        backend = get_backend()
        try:
            backend.update_workspace(model=workspace)
        except Exception as ex:
            raise DataAPIException('Error updating workspace {}'.format(ex),
                                   code='error',
                                   status_code=status.HTTP_500_INTERNAL_SERVER_ERROR) from ex

        serializer = WorkspaceSerializer(workspace, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['get'], url_path='query')
    def query(self, request, pk=None, file_pk=None): # pylint: disable=invalid-name,unused-argument
        """ Query a workspace using standard SQL """
        serializer = QuerySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sql = serializer.validated_data['sql']
        fast = serializer.validated_data['fast']
        logger.info('Querying on fast mode? %s', fast)

        # Important! Use the self.get_object() so that DRF handles the permissions
        # of this object
        try:
            workspace = self.get_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)
        logger.info('Querying %s: %s', workspace, sql)

        backend = get_backend()
        ws_object = backend.retrieve_workspace(model=workspace)
        try:
            results = ws_object.query(sql, fast=fast)
        except Exception as ex:
            raise DataAPIException('Error performing query: {}'.format(ex),
                                   code='error',
                                   status_code=status.HTTP_400_BAD_REQUEST) from ex

        # Paginate results
        page = self.paginate_queryset(results)
        if page is not None:
            output_serializer = QueryResultSerializer(page, many=True,
                                                      context={'request': request})
            return self.get_paginated_response(output_serializer.data)

        # In case there is no pagination, use a "fake" pagination where the response is:
        # {"results": [...]}
        output_serializer = QueryResultSerializer({'results':results})
        return Response(output_serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        # Important! Use the self.get_object() so that DRF handles the permissions
        # of this object
        try:
            workspace = self.get_object()
        except Http404:
            raise DataAPIException(detail='Workspace not found',
                                   code='not_found',
                                   status_code=status.HTTP_404_NOT_FOUND)
        self.perform_destroy(workspace)
        return Response(status=status.HTTP_204_NO_CONTENT)
