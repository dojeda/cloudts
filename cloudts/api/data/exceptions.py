# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


from rest_framework.exceptions import APIException


# TODO: consider a custom common exception, not this one because this is related
# to Django REST Framework
class DataAPIException(APIException):
    """Base exception for CloudTS Data API

    Intended to be used when we want to inform the consumer that something went
    wrong. The easiest way to plug this in Django Rest Framework would be to
    follow the documentation in
    http://www.django-rest-framework.org/api-guide/exceptions/#custom-exception-handling

    To specify a message, use the `detail` keyword parameter in the
    constructor.

    """
    def __init__(self, detail=None, code=None, status_code=None):
        # NB: Code is a string, status_code is a HTTP code, but what is "code"?
        # I took it from APIException and I don't understand it exactly
        super().__init__(detail, code)
        if status_code is not None:
            self.status_code = status_code
