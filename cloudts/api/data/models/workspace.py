# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" CloudTS workspace-related models """

import logging
import re
import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from jsonfield import JSONField
from rest_framework import status

from cloudts.api.data.models import get_backend
from cloudts.api.data.exceptions import DataAPIException
from cloudts.api.data.backends.exceptions import ResourceAlreadyExistsException


logger = logging.getLogger(__name__)


class Workspace(models.Model):
    """ Model representation of a CloudTS workspace """

    name = models.CharField(verbose_name='Name',
                            primary_key=True,
                            null=False,
                            blank=False,
                            max_length=256,
                            validators=[RegexValidator(regex=re.compile('[a-zA-Z][a-zA-Z0-9_-]*'),
                                                       message='Invalid workspace name')])
    start_id = models.IntegerField(verbose_name='Id of last global metadata entry',
                                   null=False)
    # TODO: perhaps on_delete=models.DO_NOTHING ?
    owner = models.ForeignKey(verbose_name='Owner',
                              to=settings.AUTH_USER_MODEL,
                              null=False,
                              on_delete=models.CASCADE)
    description = models.TextField(verbose_name='Purpose and other details',
                                   null=False,
                                   blank=True)
    date_created = models.DateTimeField(verbose_name='Date of creation',
                                        auto_now_add=True,
                                        null=False)
    families = JSONField(verbose_name='Families and versions (key: values)',
                         default={},
                         null=False)
    data_uri = models.CharField('URI for workspace data', null=True,
                                max_length=1024, blank=True)
    # Dirty: a new field to speed up queries. If a metadata is added on
    # the workspace, this field should be set to True. When scanning a workspace,
    # it is set to False so that it can automatically use the table copies instead
    # of the views
    dirty = models.BooleanField('Workspace views are out of date', default=True)

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        # The way to determine if an object already exists in Django is to check
        # its pk or to check ._state.adding
        # (http://stackoverflow.com/a/907703/227103)
        if not self._state.adding:
            # This is an update, don't do the logic of the first-time creation
            # of the workspace.
            pass
        else:
            self._init()

        return super().save(*args, **kwargs)

    def _init(self):
        """ Logic for the first-time creation of a workspace """
        # Handle empty name
        self.name = self.name or str(uuid.uuid4())
        self.dirty = True

        # Force the workspace to have a user with email
        if self.owner.email is None or not self.owner.email:
            raise DataAPIException('Refused to create workspace for a user without email')

        # Handle empty families: the cloudts family is required
        if 'cloudts' not in self.families:  # pylint: disable=unsupported-membership-test
            self.families['cloudts'] = None  # pylint: disable=unsupported-assignment-operation

        # Handle None/null families: use the latest versions
        backend = get_backend()
        available_families = backend.get_families()
        logger.info('Available families are: %s', available_families)
        errors = {}
        for name, version in self.families.items():  # pylint: disable=no-member
            # Check that family exists. If it doesn't exist it should be null
            # for the latest
            if name not in available_families and version is not None:
                errors[name] = 'Family not found'
                continue

            # Check that the version exists, or use the last one when None
            if version is None:
                if name not in available_families:
                    version = 0
                else:
                    version = max(available_families[name]) + 1
            else:
                version = int(self.families[name])  # pylint: disable=unsubscriptable-object
                if version not in available_families[name]:
                    errors[name] = 'Version not found'
                    continue

            # Update None/null to latest version
            self.families[name] = version  # pylint: disable=unsupported-assignment-operation

        if errors:
            raise ValidationError(message=errors,
                                  code=404)

        # Validate the model before creating workspace resources
        try:
            self.full_clean(exclude=('start_id',))
        except ValidationError as ex:
            raise DataAPIException(code='validation_error', detail=ex.messages)

        # Handle the backend initialization of the workspace resources
        try:
            # wbo: workspace backend object
            wbo = backend.create_workspace(model=self)
            self.data_uri = wbo.data_uri

        except ResourceAlreadyExistsException:
            raise DataAPIException(detail='Workspace already exists in backend',
                                   status_code=status.HTTP_409_CONFLICT)
        except:
            logger.error('Unexpected error when creating workspace in the backend',
                         exc_info=True)
            raise DataAPIException('Unexpected backend error')

    def delete(self, *args, **kwargs): # pylint: disable=arguments-differ
        backend = get_backend()
        backend.delete_workspace(model=self)
        return super().delete(*args, **kwargs)

    def get_data_url(self):
        """Get the URL where the user can put data to later send to CloudTS

        This is a sort of equivalent concept to the "data_temp" directory
        """
        backend = get_backend()
        ws_object = backend.retrieve_workspace(model=self)
        return ws_object.data_uri

    def __str__(self):
        fmt = 'Workspace {name} by {owner}'
        return fmt.format(owner=self.owner.get_username(),  # pylint: disable=no-member
                          name=self.name,)
