# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" General CloudTS Data API models and configurations """

import logging

from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.conf import settings

from cloudts.api.data.backends import get_backend


logger = logging.getLogger(__name__)


@receiver(pre_save, sender=settings.AUTH_USER_MODEL)
def new_user_backend(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Prepare backends for the creation of a new user """
    # logger.debug('new_user pre_save signal sender=%s instance=%s kwargs=%s',
    #              sender, instance, kwargs)
    # TODO: discern between a creation and an update
    backend = get_backend()
    backend.new_user(instance.get_username())
