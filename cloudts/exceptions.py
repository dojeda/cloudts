# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

import logging

from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler
from rest_framework.response import Response

from .api.data.exceptions import DataAPIException
from .api.job.exceptions import JobAPIException


logger = logging.getLogger(__name__)


def custom_exception_handler(exc, context):

    if not isinstance(exc, (DataAPIException, JobAPIException)):
        logger.debug('Handled the following exception:', exc_info=exc)

    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Manage the response here to have a homogeneous error response. For
    # example, the current code does not have the same structure when it's a
    # validation error.
    if isinstance(exc, (DataAPIException, JobAPIException, APIException)):
        response.data = exc.get_full_details()
        response.status_code = exc.status_code
        if 'code' not in response.data:
            logger.warning('There is no "code" in the exception details')

    elif exc is None:
        data = {
            'code': 'unknown_error',
            'detail': '{0}: {1}'.format(type(exc).__name__, exc),
        }
        response = Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return response
