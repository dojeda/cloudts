# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
""" Celery configuration """

from celery.schedules import crontab

broker_url = 'amqp://guest:guest@rabbitmq:5672//'
accept_content = ['application/json']
task_serializer = 'json'
result_serializer = 'json'
# If the timezone is changed, remember to change the settings.TIME_ZONE. I don't
# know how to link these two values in a safe way.
timezone = 'Europe/Paris'
beat_schedule = {
    # 'Update waiting tasks every 5 second': {
    #     'task': 'cloudts.api.job.tasks.update_waiting_jobs',
    #     'schedule': 15, # TODO make this a setting
    # },

    # 'Some crontab-like task': {
    #     'task': 'some_task',
    #     'schedule': crontab(minute=0, hour=0, day_of_week='*/1'),  # every day at midnight
    # }
}
# NOTE to the future: the following result_backend values *do not* work correctly!
# result_backend = 'db+postgresql://user:password@hostname/database'
# result_backend = 'db+postgresql:///django-db'
result_backend = 'django-db'

# # For development purposes (a worker that does not run inside docker-compose)
# import sys
# from django.conf import settings
# if 'worker' in sys.argv:
#     broker_url = 'amqp://guest:guest@localhost:5672//'
#     settings.DATABASES['default']['HOST'] = 'localhost'
