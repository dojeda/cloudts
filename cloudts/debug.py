# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#


import logging

logger = logging.getLogger(__name__)


# Taken from https://gist.github.com/defrex/6140951
def pretty_request(request):
    headers = ''
    for header, value in request.META.items():
        if not header.startswith('HTTP'):
            continue
        header = '-'.join([h.capitalize() for h in header[5:].lower().split('_')])
        headers += '{}: {}\n'.format(header, value)

    return (
        '{method} HTTP/1.1\n'
        'URL: {url}\n'
        'Secure? {secure}\n'
        'Content-Length: {content_length}\n'
        'Content-Type: {content_type}\n'
        '{headers}\n\n'
        '{body}'
    ).format(
        method=request.method,
        url=request.path,
        secure=request.is_secure(),
        content_length=request.META.get('CONTENT_LENGTH', None),
        content_type=request.META.get('CONTENT_TYPE', None),
        headers=headers,
        body=request.body[:1024] + (b'[... truncated ...]'
                                    if len(request.body) > 1024
                                    else b'')
    )


def pretty_response(response):
    content_length = len(response.content)
    return (
        '{code} ({phrase})\n'
        'Content ({length} bytes):\n{content}'
        ).format(
            code=response.status_code,
            phrase=response.reason_phrase,
            length=content_length,
            content=response.content[:1024] + (b'[... truncated ...]'
                                               if content_length > 1024
                                               else b'')
        )


class DebugMiddleware(object):
    """ A middleware that logs requests and responses

    Enable this middleware by adding it to `MIDDLEWARE' on settings.py.

    """

    def __init__(self, get_response):
        logger.info('DebugMiddleware is enabled %s', self)
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        logger.debug('Request is:\n%s', pretty_request(request))

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        logger.debug('Response is:\n%s', pretty_response(response))

        return response
