# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#
"""Custom loader for Celery

Needed due to a rare bug where a task would get an invalid connection to the
database and raise a InterfaceError

"""
import logging

from celery.loaders.app import AppLoader
from django import db

logger = logging.getLogger(__name__)


class CustomLoader(AppLoader):
    """Custom loader that cleans unusable DB connections"""
    def on_task_init(self, task_id, task):
        """Called before every task."""
        logger.debug('CustomLoader on_task_init')
        for conn in db.connections.all():
            conn.close_if_unusable_or_obsolete()
        super().on_task_init(task_id, task)
