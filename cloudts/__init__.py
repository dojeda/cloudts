# -*- coding: utf-8 -*-
#
# This source code is licensed under the 3-Clause BSD license found in the
# LICENSE file in the root directory of this source tree
#

# Manage the fact that cloudts and cloudts.client are under the same package
# name...
try:
    import pkg_resources
    pkg_resources.declare_namespace(__name__)
except ImportError:
    import pkgutil
    __path__ = pkgutil.extend_path(__path__, __name__)

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions

from .celery import app as celery_app
__all__ = ['__version__', 'celery_app']
