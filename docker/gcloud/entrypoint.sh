#!/bin/bash

set -e

if [[ -z "${GOOGLE_CREDENTIALS}" ]]
then
    echo "You need to define the JSON_CREDENTIALS environment variable when "
    echo "running this Docker image. Assuming your JSON file is at /some/dir/file.json"
    echo "Try the following:"
    echo "docker run -it --volume /some/dir:/tmp/credentials:ro --env GOOGLE_CREDENTIALS=/tmp/credentials/file.json ..."
    exit 1
fi

gcloud auth activate-service-account --key-file=${GOOGLE_CREDENTIALS}

${*}
