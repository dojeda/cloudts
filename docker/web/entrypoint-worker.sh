#!/bin/bash

set -e

# No need to nc -z rabbitmq 5672 because the celery worker code already handles
# the waiting of the rabbitmq server

# Normal case:
# celery worker \
#        --app cloudts \
#        --loglevel DEBUG --logfile /var/log/worker.log \
#        --concurrency 1

# Development case with auto reload when code changes:
python manage.py celery_autoreload_worker

# For internal debugging that can be attached with docker-compose
# TODO: Maybe we should use this one since kubernetes or docker-compose handle
# the concurrency.
# celery worker \
#        --app cloudts \
#        --loglevel DEBUG --logfile /var/log/worker.log \
#        --pool solo
