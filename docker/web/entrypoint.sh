#!/bin/bash

set -e
#set -x

#DIR=$(dirname "$0")

# The environment will be provided by docker --env=... or by the environment
# section in the docker-compose.yml.

mkdir -p /srv/cloudts

echo "Waiting for the database..."
until nc -z db 5432
do
    sleep 1
done
echo "Database is available"

echo "Skipped collect/upload static files (development mode)"
# # Collect static files
# echo "Collecting static files"
# python manage.py collectstatic --noinput

# This should be done manually, check the deployment/README.md
# echo "Uploading static files to Google bucket"
# gcloud auth activate-service-account --key-file=${GOOGLE_CREDENTIALS:-/code/conf/credentials.json}
# gsutil -m rsync -r -d /var/www/cloudts/static/ gs://${STATIC_BUCKET_NAME}/static

# Apply database migrations
echo "Applying database migrations..."
python manage.py makemigrations
python manage.py migrate

# Create superuser
echo "Creating superusers..."
python manage.py shell <<EOF
import cloudts._create_admin;
cloudts._create_admin.main();
EOF

# Start server
echo "Starting server..."
if [[ -z $USE_GUNICORN ]]
then
    echo "Serving with Python..."
    python manage.py runserver 0.0.0.0:8000
else
    echo "Serving with Gunicorn..."
    gunicorn --bind 0.0.0.0:8000 --timeout 300 cloudts.wsgi
fi
